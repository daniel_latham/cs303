# -*- coding: utf-8 -*-
"""
SYNOPSIS

    python BinaryTree.py

DESCRIPTION

    BinaryTree class that implements the data structure for binary tree with data and references to two of its child
    Makes a binary tree from elements in the array such that if 'i' is the parent then 2i and 2i+1 are the children.
    Elements with value -1 means that node doesn’t exists.
    Implemented a method to find the height of a binary tree.
    Implemented a method to print the path that has largest sum from the root to the leaf.

EXAMPLES

    python BinaryTree.py

EXIT STATUS

    0

AUTHOR

    Daniel Latham <l3li3l@uab.edu>

LICENSE

    This script is in the public domain, free from copyrights or restrictions.

VERSION

    1.2 Added Driver class because this is asked of us in the implementation
    1.1 Filled in some documentation, fixed typos, removed extra stuff, completed lab report
    1.0 Initial Release

"""

__author__ = 'Daniel Latham'

###Node class serves as wrapper for data in BinaryTree
class Node(object):
    ###Declares member variables
    ###Basic initialization method requiring data
    def __init__(self, data):
        assert data is not None
        self.left = None
        self.right = None
        self.data = data

    ###Returns boolean if/not this.isLeaf element
    def isLeaf(self):
        if self.right or self.left:
            return False
        return True

    ###Returns a string with the paths(L for left turn, R for right turn) of each element from root to element
    def toStringPreOrder(self, pathString = ""):
        treeString = pathString + " : " + str(self.data) + "\n";

        if self.left is not None:
            treeString += self.left.toStringPreOrder(pathString + "L")

        if self.right is not None:
            treeString += self.right.toStringPreOrder(pathString + "R")

        return treeString

    ###Recursive implementation to find the largest sum. This is very similar to the toStringPreOrder in that
    ###it returns all paths, whether they are from root to leaf or not. Because math, this is not a problem, for any
    ###path from root to leaf is more than any path from root to leaf-1
    ###Returns a string representation of the addition of all elements in a path
    def largestSum(self, pathString = ""):
        treeString = pathString + str(self.data) + "\n";

        if self.left is not None:
            treeString += self.left.largestSum(pathString + str(self.data) + "+")

        if self.right is not None:
            treeString += self.right.largestSum(pathString + str(self.data) + "+")

        return treeString

    ###Recursive implementation to find the maximum height of the tree in O(N) time
    def height(self, node):
        if node == None:
            return -1
        return 1+max(node.height(node.left), node.height(node.right));


###BinaryTree class holds the root Node and manipulates methods in Node class to return human readable information
###about the tree
class BinaryTree(object):

    ###Basic initialization method, lst can be none but there is no way to add elements to the tree otherwise
    def __init__(self, lst = None):
        self.root = None
        if lst is not None:
            self.root = Node(lst[0])
            self.__createBinaryTree(self.root, 0, lst)

    ###Private method that creates the binary tree from a list of integers A
    def __createBinaryTree(self, node, i, A):

        if i >= len(A)-1:
            return

        if A[2*i+1] != -1:
            newNode = Node(A[2*i+1])
            node.left = newNode
            self.__createBinaryTree(newNode, 2*i+1, A)

        if A[2*i + 2] != -1:
            newNode = Node(A[2*i + 2])
            node.right = newNode
            self.__createBinaryTree(newNode, 2*i + 2, A)

    ###Prints the binary tree by calling on the toStringPreOrder method in the root node
    def _print(self):
        print self.root.toStringPreOrder("")

    ###Prints the height of the binary tree by calling on the height method in the root node
    def _height(self):
        return self.root.height(self.root)

    ###Calls upon the root's largestSum function
    ###Returns largest sum from root to leaf in the format:
    ### [sum, root.data, leaf.data, path]
    def _largestSum(self):
        lines = self.root.largestSum()
        paths = self.root.toStringPreOrder()
        k = 0
        lnlines = lines.split("\n")
        keyval = {}
        for line in lnlines:
            if len(line) is not 0:
                keyval[k] = eval(line) #Evaluates x+y+z data values in line and adds to keyval dictionary
                k+=1

        mx = -1
        mxnum = -1
        #Iterates over each value to find the maximum key/value pair
        for value in keyval:
            temp = keyval.get(value)
            tempnum = value
            if temp > mx:
                mx = temp
                mxnum = tempnum

        leaf = int(lnlines[mxnum].split("+")[len(lnlines[mxnum].split("+"))-1])

        lnpaths = paths.split("\n")
        keyval = {}
        for line in lnpaths:
            temp = line.split(":")
            if len(temp) > 1:
                keyval[int(temp[1].lstrip(" "))] = temp[0].rstrip(" ")

        return [mx, self.root.data, leaf, keyval[leaf]]




###Driver Class for the binary tree
###Class because python is object oriented(sometimes) and portable
class Driver(object):

    def __init__(self):
        pass
    ###Tests the binary tree
    def test(self):
        print """Initialize new BinaryTree with values: [6,11,4,10,7,2,9,-1,-1,
                                            -1,12,-1,1,5,8,-1,-1,-1,-1,
                                            -1,-1,-1,-1,-1,-1,0,-1,-1,
                                            -1,-1,-1,-1,-1,-1,-1,-1,-1,
                                            -1,-1,-1,-1,-1,-1,-1,-1,-1,
                                            -1,-1,-1,-1,-1,-1,3] """
        bintree = BinaryTree([6,11,4,10,7,2,9,-1,-1,
                              -1,12,-1,1,5,8,-1,-1,-1,-1,
                              -1,-1,-1,-1,-1,-1,0,-1,-1,
                              -1,-1,-1,-1,-1,-1,-1,-1,-1,
                              -1,-1,-1,-1,-1,-1,-1,-1,-1,
                              -1,-1,-1,-1,-1,-1,3])
        print "Printed BinaryTree: "
        bintree._print()
        print "Height == ",bintree._height()
        sumf = bintree._largestSum()
        print "Largest sum == %d from root node %d to leaf node %d" % (sumf[0], sumf[1], sumf[2])

        print "Path from root node %d to leaf node %d is : %s" % (sumf[1], sumf[2], sumf[3])


def __main__():
    d = Driver()
    d.test()


if __name__ == "__main__":
    __main__()