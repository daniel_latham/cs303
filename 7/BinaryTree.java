package lab7;

public class BinaryTree {

	public static Node root;
	
	public BinaryTree(){}
	
	public void makeBinaryTree(int[] A){
		
		if (A.length > 0 && A[0] != -1){
			root = new Node();
			root.data = A[0];
			createBinaryTree(root, 0, A);
		}
	}
	
	private void createBinaryTree(Node node, int i, int[] A){
		
		if (i >= A.length-1) return;
		
		if (A[2*i + 1] != -1) {
			
			Node newNode = new Node();
			newNode.data = A[2*i + 1];
			node.left = newNode;
			createBinaryTree(newNode, 2*i + 1, A);
		}
		if (A[2*i + 2] != -1) {
			Node newNode = new Node();
			newNode.data = A[2*i + 2];
			node.right = newNode;
			createBinaryTree(newNode, 2*i + 2, A);
		}
	}
	
	
	public static void main(String[] args){
		
		int[] A = {6,11,4,10,7,2,9,-1,-1,
				-1,12,-1,1,5,8,-1,-1,-1,-1,
				-1,-1,-1,-1,-1,-1,0,-1,-1,
				-1,-1,-1,-1,-1,-1,-1,-1,-1,
				-1,-1,-1,-1,-1,-1,-1,-1,-1,
				-1,-1,-1,-1,-1,-1,3};
		BinaryTree tree = new BinaryTree();
		tree.makeBinaryTree(A);
		System.out.println(tree.root.toStringPreOrder(""));
		
	}
	
}
