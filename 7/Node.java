package lab7;

public class Node {

	public Node left;
	public Node right;
	public int data;
	
	public Node(){}
	
	public String toStringPreOrder(String pathString) {
		String treeString = pathString + " : " + data + "\n";

		if (left != null) {
		treeString += left.toStringPreOrder(pathString + "L");
		}

		if (right != null) {
		treeString += right.toStringPreOrder(pathString + "R");
		}

		return treeString;
		}
}
