package edu.uab.cis.l3li3l.lab1;

import java.util.Collections;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.*;

/**
 * Driver for searching algorithms. To use this program, execute 
 * $: java -ea AlgorithmDriver /path/to/input.txt 25 
 * This drive will import a list of integers and place them in an array in the same order
 * they are given for the Linear Search as well as a sorted array for the Binary Search.
 * @author Daniel Latham
 *
 */
public class AlgorithmDriver {
	
	/**
	 * Tests the Linear Search Algorithm and returns the place that the number is found
	 * or -1 if the number is not found and the time it took in an Array List
	 * @param l int array of all the elements
	 * @param k element to find
	 * @return ArrayList
	 */
	public ArrayList<Number> testLinear(int[] l, int k){
		LinearSearch ll = new LinearSearch();
		long beg = System.nanoTime();
		int place = ll.search(l, k);
		long end = System.nanoTime();
		long time = ((end-beg)/1000);
		ArrayList<Number> ret = new ArrayList<>();
		ret.add(place);
		ret.add(time);
		return ret;
	}

	/**
	 * Tests the Binary Search Algorithm and returns the place that the number is found
	 * or -1 if the number is not found and the time it took in an Array List
	 * @param l int array of all the elements
	 * @param k element to find
	 * @return ArrayList ret with ret[0] == place && ret[1] == time
	 */
	public ArrayList<Number> testBinary(int[] l, int k) {
		BinarySearch ll = new BinarySearch();
		long beg = System.nanoTime();
		int place = ll.search(l, k);
		long end = System.nanoTime();
		long time = ((end-beg)/1000);
		ArrayList<Number> ret = new ArrayList<>();
		ret.add(place);
		ret.add(time);
		return ret;
	}
	
}
