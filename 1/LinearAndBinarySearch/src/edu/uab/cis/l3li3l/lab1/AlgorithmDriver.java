package edu.uab.cis.l3li3l.lab1;

import java.util.ArrayList;

/**
 * Driver for searching algorithms
 *
 */
public class AlgorithmDriver {
	
	/**
	 * Tests the Linear Search Algorithm and returns the place that the number is found
	 * or -1 if the number is not found and the time it took in an Array List
	 * @param l int array of all the elements
	 * @param k element to find
	 * @return ArrayList
	 */
	public ArrayList<Number> testLinear(int[] l, int k){
		LinearSearch ll = new LinearSearch();
		long beg = System.nanoTime();
		int place = ll.search(l, k);
		long end = System.nanoTime();
		long time = ((end-beg)/1000);
		ArrayList<Number> ret = new ArrayList<>();
		ret.add(place);
		ret.add(time);
		return ret;
	}

	/**
	 * Tests the Binary Search Algorithm and returns the place that the number is found
	 * or -1 if the number is not found and the time it took in an Array List
	 * @param l int array of all the elements
	 * @param k element to find
	 * @return ArrayList ret with ret[0] == place && ret[1] == time
	 */
	public ArrayList<Number> testBinary(int[] l, int k) {
		BinarySearch ll = new BinarySearch();
		long beg = System.nanoTime();
		int place = ll.search(l, k);
		long end = System.nanoTime();
		long time = ((end-beg)/1000); //nano becomes micro
		ArrayList<Number> ret = new ArrayList<>();
		ret.add(place);
		ret.add(time);
		return ret;
	}
	
}
