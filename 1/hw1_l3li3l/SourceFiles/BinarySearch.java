package edu.uab.cis.l3li3l.lab1;

public class BinarySearch implements SearchAlgo {

	@Override
	public int search(int[] l, int k) {
		return binSearch(l,k,0,l.length-1);
	}
	
	private int binSearch(int A[], int key, int imin, int imax)
	{
		// test if array is empty
		if (imax < imin)
		{
			// set is empty, so return value showing not found
			return -1;
		}
		// calculate midpoint to cut set in half
		int imid = (imin+imax)/2;

		// three-way comparison
		if (A[imid] > key)
		{
			// key is in lower subset
			return binSearch(A, key, imin, imid - 1);
		}
		else if (A[imid] < key)
		{	// key is in upper subset
			return binSearch(A, key, imid + 1, imax);
		}
		else
		{	// key has been found
			return imid;
		}
	}

}
