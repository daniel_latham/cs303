package edu.uab.cis.l3li3l.lab1;

import java.util.Collections;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.*;

/**
 * Driver for searching algorithms. To use this program, execute 
 * $: java -ea AlgorithmDriver /path/to/input.txt 25 
 * This drive will import a list of integers and place them in an array in the same order
 * they are given for the Linear Search as well as a sorted array for the Binary Search.
 * @author Daniel Latham
 *
 */
public class AlgorithmDriver {
	
	/**
	 * Tests the Linear Search Algorithm and returns the place that the number is found
	 * or -1 if the number is not found and the time it took in an Array List
	 * @param l int array of all the elements
	 * @param k element to find
	 * @return ArrayList
	 */
	public ArrayList<Number> testLinear(int[] l, int k){
		LinearSearch ll = new LinearSearch();
		long beg = System.nanoTime();
		int place = ll.search(l, k);
		long end = System.nanoTime();
		long time = ((end-beg)/1000);
		ArrayList<Number> ret = new ArrayList<>();
		ret.add(place);
		ret.add(time);
		return ret;
	}

	/**
	 * Tests the Binary Search Algorithm and returns the place that the number is found
	 * or -1 if the number is not found and the time it took in an Array List
	 * @param l int array of all the elements
	 * @param k element to find
	 * @return ArrayList ret with ret[0] == place && ret[1] == time
	 */
	public ArrayList<Number> testBinary(int[] l, int k) {
		BinarySearch ll = new BinarySearch();
		long beg = System.nanoTime();
		int place = ll.search(l, k);
		long end = System.nanoTime();
		long time = ((end-beg)/1000);
		ArrayList<Number> ret = new ArrayList<>();
		ret.add(place);
		ret.add(time);
		return ret;
	}
	
	/**
	 * Imports variables and calls on tests
	 * @param one Path to input.txt
	 * @param two String representation of int that needs to be found
	 */
	private void importVars(String one, String two)
	{
		File f = null;
		int k = 0;
		try {
			f = new File(one);
			k = Integer.parseInt(two);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Scanner s = null;
		try {
			s = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		ArrayList<Integer> all = new ArrayList<>();
		//Adds all elements in the file to the arraylist
		while (s.hasNextInt()) {
			all.add(s.nextInt());
		}
		
		ArrayList<Integer> allSorted = new ArrayList<>();
		//allSorted is a copy of all
		allSorted.addAll(all);
		//Sorts allSorted in ascending order
		Collections.sort(allSorted);
		
		//Instantiates two new arrays of ints to be used for tests
		int[] l = new int[all.size()];
		int[] lSorted = new int[allSorted.size()];
		
		//Adds all elements in arraylists to arrays
		for (int i = 0; i < all.size(); i++){
			l[i] = all.get(i);
			lSorted[i] = allSorted.get(i);
		}
		
		//Tests Linear and Binary search algorithm then exits
		testLinear(l, k);
		testBinary(lSorted, k);
	}
	/*
	public static void main(String[] args)
	{
		assert args.length == 2: "Expecting 2 inputs, exiting";
		importVars(args[0], args[1]);
	} */
}
