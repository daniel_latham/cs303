package edu.uab.cis.l3li3l.lab1;

public interface SearchAlgo {
	
	/**
	 * Searches an array of integers for a given value and returns the 
	 * place in the array that the value is found
	 * @param l array of ints to search
	 * @param k int value to find
	 * @return Value of the place that k is found
	 */
	public int search(int[] l, int k);

}
