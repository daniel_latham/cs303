package edu.uab.cis.cs303.l3li3l.hw9;

public class RBNode<K extends Comparable<? super K>, V>{

	public RBNode<K,V> parent;
	public RBNode<K,V> left;
	public RBNode<K,V> right;
	public K key;
	public V value;
	public boolean color;
	
	public RBNode(){}
	
}
