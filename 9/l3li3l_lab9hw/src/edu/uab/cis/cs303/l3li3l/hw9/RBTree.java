package edu.uab.cis.cs303.l3li3l.hw9;

public class RBTree<K extends Comparable<? super K>, V>{

	public RBNode<K,V> root;
	public RBNode<K,V> nil = new RBNode<>(); 
	
	public RBTree(){
		nil.color = false;
		root = nil;
	}
	
	public void rBInsert(K data, V value){
		
		RBNode<K,V> z = new RBNode<>();
		z.key = data;
		z.value = value;
		
		RBNode<K,V> y = nil;
		RBNode<K,V> x = this.root;
		
		while (x != nil){
			y = x;
			if (z.key.compareTo(x.key) < 0){
				x = x.left;
			}
			else {x = x.right;}
		}
		
		z.parent = y;
		
		if (y == nil){
			this.root = z;
		}
		else if (z.key.compareTo(y.key)< 0){
			y.left = z;
		}
		else {y.right = z;}
		
		z.left = nil;
		z.right = nil;
		z.color = true;
		rBInsertFixup(z);
	}
	
	public void leftRotate(RBNode<K,V> x){
		
		RBNode<K,V> y = x.right;
		x.right = y.left;
		
		if (y.left != nil){ y.left.parent = x; }
		y.parent = x.parent;
		if (x.parent == nil){ this.root = y; }
		else if (x == x.parent.left) { x.parent.left = y; }
		else { x.parent.right = y; }
		y.left = x;
		x.parent = y;
	}
	
	public void rightRotate(RBNode<K,V> x){
		
		RBNode<K,V> y = x.left;
		x.left = y.right;
		
		if (y.right != nil){ y.right.parent = x; }
		y.parent = x.parent;
		if (x.parent == nil){ this.root = y; }
		else if (x == x.parent.right) { x.parent.right = y; }
		else { x.parent.left = y; }
		y.right = x;
		x.parent = y;
	}

	public void rBInsertFixup(RBNode<K,V> z){
		
		RBNode<K,V> y;
		
		while (z.parent.color == true && z.parent.parent != null){
			
			if (z.parent == z.parent.parent.left){
				y = z.parent.parent.right;
				if (y.color == true){
					z.parent.color = false;
					y.color = false;
					z.parent.parent.color = true;
					z = z.parent.parent;
				}
				else{
					if (z == z.parent.right){
				
					z = z.parent;
					this.leftRotate(z);
				}
				z.parent.color = false;
				z.parent.parent.color = true;
				rightRotate(z.parent.parent);
				}
			}
			else {
				y = z.parent.parent.left;
				if (y.color == true){
					z.parent.color = false;
					y.color = false;
					z.parent.parent.color = true;
					z = z.parent.parent;
				}
				else {
					if (z == z.parent.left){
						z = z.parent;
						this.rightRotate(z);
					}
					z.parent.color = false;
					z.parent.parent.color = true;
					leftRotate(z.parent.parent);
				}
			}
		}
		this.root.color = false;
	}
	public void inOrderTraversal(){
		
		traversalHelper(this.root);
		
	}
	
	public void traversalHelper(RBNode<K,V> x){
		
		if (x != nil){
			traversalHelper(x.left);
			System.out.print(x.key + " " + x.value + " " + x.color + " ");
			traversalHelper(x.right);
		}	
	}
	

	public RBNode<K,V> search(K key){
		
		return searchHelper(this.root, key);
		
	}
	
	private RBNode<K,V> searchHelper(RBNode<K,V> x, K key){
		
		if (x == null || x.key.equals(key)){
			return x;
		}
		
		if (key.compareTo(x.key) < 0){
			return searchHelper(x.left, key);
		}
		
		return searchHelper(x.right, key);
	}
}
