# -*- coding: utf-8 -*-
"""
SYNOPSIS

    TODO helloworld [-h,--help] [-v,--verbose] [--version]

DESCRIPTION

    TODO This describes how to use this script. This docstring
    will be printed by the script if there is an error or
    if the user requests help (-h or --help).

EXAMPLES

    TODO: Show some examples of how to use this script.

EXIT STATUS

    1

AUTHOR

    Daniel Latham <l3li3l@uab.edu>

LICENSE

    This script is in the public domain, free from copyrights or restrictions.

VERSION

    
"""

__author__ = 'Daniel Latham'
