# -*- coding: utf-8 -*-
"""
SYNOPSIS

    python RedBlackTree.py

DESCRIPTION



EXAMPLES

    python RedBlackTree.py

EXIT STATUS

    0

AUTHOR

    Daniel Latham <l3li3l@uab.edu>

LICENSE

    This script is in the public domain, free from copyrights or restrictions.

VERSION

    1.0 Initial Release

"""

__author__ = 'Daniel Latham'

class Node(object):
    """
    RBTNode: left and right are children
    data is any comparable object
    color is the color of the node:
        False = RED
        True = BLACK
    """
    def __init__(self, data):
        """
        Node constructor

        :param data: node data object
        """
        self.left = None
        self.right = None
        self.color = False
        self.p = None #PARENT node
        self.data = data


    def inorderTreeWalk(self):
        """
        Simple printing algorithm that prints the leftmost element, then leftmost+1, ... rightmost element

        :return: None
        """
        if self.left and self.left.left is not self.left:
            self.left.inorderTreeWalk()
        clr = "none"
        if self.color == False:
            clr = "RED"
        else:
            clr = "BLACK"
        print str(self.data)+":"+clr,
        if self.right and self.right.right is not self.right:
            self.right.inorderTreeWalk()

class RedBlackTree(object):
    """
    RedBlackTree implementation
    Used to implement lookup tables and dynamic sets
    Stores data items, known as keys, and allows fast insertion and deletion of such keys as well as checking whether
    a key is present in a tree
    root = root Node object to traverse
    NIL = self referencing node
    :param lst: List object of data to input
    """
    def __init__(self, lst = None):
        """
        :param lst: List object of data to input
        :return: None
        """
        self.NIL = Node(None)

        self.root = self.NIL

        self.NIL.left = self.NIL
        self.NIL.right = self.NIL
        self.NIL.p = self.NIL
        self.NIL.color = True

        if lst is not None:
            for data in lst:
                self.insert(data)

    def leftRotate(self, x):
        y = x.right
        x.right = y.left
        if y.left is not self.NIL:
            y.left.p = x
        y.p = x.p
        if x.p is self.NIL:
            self.root = y
        elif x is x.p.left:
            x.p.left = y
        else:
            x.p.right = y
        y.left = x
        x.p = y

    def rightRotate(self, x):
        y = x.left
        x.left = y.right
        if y.right is not self.NIL:
            y.right.p = x
        y.p = x.p
        if x.p is self.NIL:
            self.root = y
        elif x is x.p.right:
            x.p.right = y
        else:
            x.p.left = y
        y.right = x
        x.p = y

    def insert(self, z):
        ##if z is not node
        if z.__class__ is not Node.__class__:
            z = Node(z)

        y = self.NIL
        x = self.root
        while x is not self.NIL:
            y = x
            if z.data < x.data:
                x = x.left
            else:
                x = x.right
        z.p = y
        if y is self.NIL:
            self.root = z
        elif z.data < y.data:
            y.left = z
        else:
            y.right = z
        z.left = self.NIL
        z.right = self.NIL
        z.color = False
        self.__fixup(z)
        #moved from __fixup()
        self.root.color = True
        #print "TEST"+str(self.root.color)

    def __fixup(self, z):
        if z is self.root or z.p is self.root:
            return
        while z.p.color is False and z.p.p is not None:
            print "Line 163"
            if z.p is z.p.p.left:
                print "Line 165"
                y = z.p.p.right
                if y.color is False:
                    print "Line 168"
                    z.p.color = True
                    y.color = True
                    z.p.p.color = False
                    z = z.p.p
                else:
                    if z is z.p.right:
                        print "Line 175"
                        z = z.p
                        self.leftRotate(z)
                z.p.color = True
                z.p.p.color = False
                self.rightRotate(z.p.p)
            else:
                #TODO: "Same as then clause with 'right' and 'left' exchanged"
                #print "END HERE"
                print "Line 185"
                y = z.p.p.left
                if y.color is False:
                    print "Line 188"
                    z.p.color = True
                    y.color = True
                    z.p.p.color = False
                    z = z.p.p
                else:
                    if z is z.p.left:
                        print "Line 195"
                        z = z.p
                        self.rightRotate(z)
                z.p.color = True
                z.p.p.color = False
                self.leftRotate(z.p.p)

    def inorderTreeWalk(self):
        """
        Prints binary search tree's string representation inorder by calling on root Node's inorderTreeWalk
        :return: None
        """
        assert self.root is not None
        self.root.inorderTreeWalk()
        print ##Extra line




class Driver(object):

    def __init__(self):
        self.tree = RedBlackTree()

    def test(self):

        self.tree.insert(10)
        self.tree.inorderTreeWalk()
        self.tree.insert(9)
        self.tree.inorderTreeWalk()
        self.tree.insert(8)
        self.tree.inorderTreeWalk()


        """
        self.tree.insert(5)
        self.tree.inorderTreeWalk()
        #self.tree.insert(4)
        #self.tree.inorderTreeWalk()
        self.tree.insert(6)
        self.tree.inorderTreeWalk()
        self.tree.insert(4)
        self.tree.inorderTreeWalk()
        self.tree.insert(3)
        self.tree.inorderTreeWalk()


        ##Basic RBTree printed
        print
        print "Pushed black down from root"
        print str(self.tree.root.data)+":"+str(self.tree.root.color)+":ROOT"
        print str(self.tree.root.left.data)+":"+str(self.tree.root.left.color)+":L",\
            str(self.tree.root.right.data)+":"+str(self.tree.root.right.color)+":R"
        print str(self.tree.root.left.left.data)+":"+str(self.tree.root.left.left.color)+":LL"

        """
        print
        print "Init new tree [3, 7, 10, 12]"#, 12, 14, 15, 16, 17, 19, 20, 21, 23, 26, 28, 30, 35, 38, 39, 41, 47]"
        self.tree = RedBlackTree([3, 7, 10, 12])#, 12, 14, 15, 16, 17, 19, 20, 21, 23, 26, 28, 30, 35, 38, 39, 41, 47])
        self.tree.inorderTreeWalk()


def __main__():
    d = Driver()
    d.test()


if __name__ == "__main__":
    __main__()