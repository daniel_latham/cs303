# -*- coding: utf-8 -*-
"""
SYNOPSIS

    python BinarySearchTree.py

DESCRIPTION

    A node-based binary tree data structure where each node has a comparable key (and an associated value) and satisfies
    the restriction that the key in any node is larger than the keys in all nodes in that node's left sub-tree and
    smaller than the keys in all nodes in that node's right sub-tree. Each node has no more than two child nodes. Each
    child must either be a leaf node or the root of another binary search tree. The left sub-tree contains only nodes
    with keys less than the parent node; the right sub-tree contains only nodes with keys greater than the parent node.
    BSTs are also dynamic data structures, and the size of a BST is only limited by the amount of free memory in the
    operating system. The main advantage of binary search trees is that it remains ordered, which provides quicker
    search times than many other data structures. The common properties of binary search trees are as follows:

        *One node is designated the root of the tree.
        *Each internal node contains a key, and can have up to two subtrees.
        *The leaves (final nodes) of the tree contain no key.
        *Each subtree is itself a binary search tree.
        *The left subtree of a node contains only nodes with keys strictly less than the node's key.
        *The right subtree of a node contains only nodes with keys strictly greater than the node's key.

    Generally, the information represented by each node is a record rather than a single data element. However, for
    sequencing purposes, nodes are compared according to their keys rather than any part of their associated records.

    The major advantage of binary search trees over other data structures is that the related sorting algorithms and
    search algorithms such as in-order traversal can be very efficient; they are also easy to code.

    Binary search trees are a fundamental data structure used to construct more abstract data structures such as sets,
    multisets, and associative arrays. Some of their disadvantages are as follows:

        *The shape of the binary search tree totally depends on the order of insertions, and it can be degenerated.
        *When inserting or searching for an element in binary search tree, the key of each visited node has to be
            compared with the key of the element to be inserted or found, i.e., it takes a long time to search an
            element in a binary search tree.
        *The keys in the binary search tree may be long and the run time may increase.
        *After a long intermixed sequence of random insertion and deletion, the expected height of the tree approaches
            square root of the number of keys which grows much faster than log n.

    ref: [http://en.wikipedia.org/wiki/Binary_search_tree#Definition]


EXAMPLES

    python BinarySearchTree.py

EXIT STATUS

    0

AUTHOR

    Daniel Latham <l3li3l@uab.edu>

LICENSE

    This script is in the public domain, free from copyrights or restrictions.

VERSION

    1.1 Fixed some documentation
    1.0 Initial Release

"""

__author__ = 'Daniel Latham'

class Node(object):
    """
    BSTNode: left and right are children
    data is any comparable object
    """
    def __init__(self, data):
        """
        Node constructor

        :param data: node data object
        """
        self.left = None
        self.right = None
        self.data = data
        
    def insert(self, data):
        """
        Insert new node with data

        :param data: node data object to insert
        """
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.left = Node(data)
                else:
                    self.left.insert(data)
            elif data > self.data:
                if self.right is None:
                    self.right = Node(data)
                else:
                    self.right.insert(data)
        else:
            self.data = data
            
    def search(self, data, parent=None):
        """
        search node containing data

        :param data: node data object to look up
        :param parent: node's parent
        :return: node and node's parent if found or None, None
        """
        if data < self.data:
            if self.left is None:
                return None, None
            return self.left.search(data, self)
        elif data > self.data:
            if self.right is None:
                return None, None
            return self.right.search(data, self)
        else:
            return self, parent

    def inorderTreeWalk(self):
        """
        Simple printing algorithm that prints the leftmost element, then leftmost+1, ... rightmost element

        :return: None
        """
        if self.left:
            self.left.inorderTreeWalk()
        print self.data,
        if self.right:
            self.right.inorderTreeWalk()

    def delete(self, data):
        """
        Deletes a specified value if it exists

        :param data: Node(data) to delete in the tree
        :return: Boolean if value was found and deleted
        """
        node, parent = self.search(data)
        #test if node exists
        if node is not None:
            children_count = node.__count()
        else:
            return False
        #node exists, so check if it has children that need to be moved
        if children_count is 0:
            if parent:
                if parent.left is node:
                    parent.left = None
                else:
                    parent.right = None
                del node
            else:
                self.data = None
        elif children_count is 1:
            if node.left:
                n = node.left
            else:
                n = node.right
            if parent:
                if parent.left is node:
                    parent.left = n
                else:
                    parent.right = n
                del node
            else:
                self.left = n.left
                self.right = n.right
                self.data = n.data
        else:
            parent = node
            successor = node.right
            while successor.left:
                parent = successor
                successor = successor.left

            node.data = successor.data

            if parent.left is successor:
                parent.left = successor.right
            else:
                parent.right = successor.right
        #node exists and was deleted
        return True

    def __count(self):
        """
        Returns number of children, integer value of 0,1, or 2

        :return: Int count of children
        """
        cunt = 0
        if self.left:
            cunt += 1
        if self.right:
            cunt += 1
        return cunt

class BinarySearchTree(object):
    """
    BinarySearchTree implementation, sometimes called an ordered or sorted binary tree
    Used to implement lookup tables and dynamic sets
    Stores data items, known as keys, and allows fast insertion and deletion of such keys as well as checking whether
    a key is present in a tree
    root = root Node object to traverse
    :param lst: List object of data to input
    """
    def __init__(self, lst = None):
        """
        :param lst: List object of data to input
        :return: None
        """
        self.root = None

        if lst is not None:
            for data in lst:
                self.insert(data)

    def search(self, data):
        """
        Returns object representation of Node(data) if found, and None if not
        :param data: data in the Node()
        :return: Node object or None
        """
        assert self.root is not None
        return self.root.search(data)[0] ##Returns node object

    def insert(self, data):
        """
        Insertion of data into binary search tree inorder
        :param data:
        :return:
        """
        if self.root is None:
            self.root = Node(data)
        else:
            self.root.insert(data)

    def inorderTreeWalk(self):
        """
        Prints binary search tree's string representation inorder by calling on root Node's inorderTreeWalk
        :return: None
        """
        assert self.root is not None
        self.root.inorderTreeWalk()
        print ##Extra line

    def delete(self, data):
        """
        Delete node with specified data value
        :param data: data the node posesses
        :return: Boolean if Node(data) was deleted successfully
        """
        assert self.root is not None
        return self.root.delete(data)




class Driver(object):

    def __init__(self):
        pass

    def test(self):
        line = "_____________________________________________________________"
        print "Test: BinarySearchTree with init array [5,4,3,6,7,2,9,1,8]"
        print line
        #b = BinarySearchTree()
        #print b.search(12)
        b = BinarySearchTree([5,4,3,6,7,2,9,1,8])
        print "inorderTreeWalk == " ,
        b.inorderTreeWalk()
        print line
        print "Inserting 22, 32, and 16"
        b.insert(22)
        b.insert(32)
        b.insert(16)
        b.inorderTreeWalk()
        print line
        print "Searching for values 7 and 73"
        print "7 should return a Node object because it exists in the tree"
        print "73 should return a None object because it doesn't exist"
        seven = b.search(7)
        seventhree = b.search(73)
        print "7 == %s" % seven
        print "73 == %s" % seventhree
        print line
        print "Deleting 7"
        print "Deletion of 7 is successful? %s" % str(b.delete(7))
        b.inorderTreeWalk()


def __main__():
    d = Driver()
    d.test()


if __name__ == "__main__":
    __main__()