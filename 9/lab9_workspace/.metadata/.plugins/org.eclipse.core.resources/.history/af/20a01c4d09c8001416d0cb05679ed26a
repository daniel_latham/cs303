package edu.uab.cis.cs303.l3li3l.hw9;

public class RBTree<K extends Comparable<? super K>, V>{

	public RBNode<K,V> root;
	public RBNode<K,V> nil = new RBNode<K,V>(); 
	
	public RBTree(){
		nil.color = false;
		root = nil;
	}
	
	public void rBInsert(K data, V value){
		
		//create new BSTNode with data = data
		RBNode<K,V> z = new RBNode<K,V>();
		z.key = data;
		z.value = value;
		
		//nil BSTNode
		RBNode<K,V> y = nil;
		//x refers to the root of this tree
		RBNode<K,V> x = this.root;
		
		//iterate through elements until finding proper parent for z, which will be y
		while (x != nil){
			y = x;
			if (z.key.compareTo(x.key) < 0){
				x = x.left;
			}
			else {x = x.right;}
		}
		
		//set the parent of z to y
		z.parent = y;
		
		//handle special case where z is first element added to the binary search tree
		if (y == nil){
			this.root = z;
		}
		//handle case where z should be left child of y
		else if (z.key.compareTo(y.key)< 0){
			y.left = z;
		}
		//handle case where z should be right child of y
		else {y.right = z;}
		
		z.left = nil;
		z.right = nil;
		z.color = true;
		rBInsertFixup(z);
	}
	
	public void leftRotate(RBNode<K,V> x){
		
		RBNode<K,V> y = x.right;
		x.right = y.left;
		
		if (y.left != nil){ y.left.parent = x; }
		y.parent = x.parent;
		if (x.parent == nil){ this.root = y; }
		else if (x == x.parent.left) { x.parent.left = y; }
		else { x.parent.right = y; }
		y.left = x;
		x.parent = y;
	}
	
	public void rightRotate(RBNode<K,V> x){
		
		RBNode<K,V> y = x.left;
		x.left = y.right;
		
		if (y.right != nil){ y.right.parent = x; }
		y.parent = x.parent;
		if (x.parent == nil){ this.root = y; }
		else if (x == x.parent.right) { x.parent.right = y; }
		else { x.parent.left = y; }
		y.right = x;
		x.parent = y;
	}

	public void rBInsertFixup(RBNode<K,V> z){
		
		RBNode<K,V> y;
		
		while (z.parent.color == true && z.parent.parent != null){
			
			if (z.parent == z.parent.parent.left){
				y = z.parent.parent.right;
				if (y.color == true){
					z.parent.color = false;
					y.color = false;
					z.parent.parent.color = true;
					z = z.parent.parent;
				}
				else{
					if (z == z.parent.right){
				
					z = z.parent;
					this.leftRotate(z);
				}
				z.parent.color = false;
				z.parent.parent.color = true;
				rightRotate(z.parent.parent);
				}
			}
			else {
				y = z.parent.parent.left;
				if (y.color == true){
					z.parent.color = false;
					y.color = false;
					z.parent.parent.color = true;
					z = z.parent.parent;
				}
				else {
					if (z == z.parent.left){
						z = z.parent;
						this.rightRotate(z);
					}
					z.parent.color = false;
					z.parent.parent.color = true;
					leftRotate(z.parent.parent);
				}
			}
		}
		this.root.color = false;
	}
	/*
	 * prints the in-order traversal of this binary search tree
	 */
	public void inOrderTraversal(){
		
		//use traversalHelper method on this.root to allow for recursive in-order traversal
		traversalHelper(this.root);
		
	}
	
	/*
	 * given a BSTNode, recursively prints the in-order traversal of the subtree rooted at this node
	 * @param x root of tree or subtree
	 */
	public void traversalHelper(RBNode<K,V> x){
		
		//iterate through all elements, printing them in order
		if (x != nil){
			traversalHelper(x.left);
			System.out.print(x.key + " " + x.value + " " + x.color + " ");
			traversalHelper(x.right);
		}	
	}
	
	/*
	 * searches for a node whose data == data
	 * @param data the data to be found in the binary search tree
	 * @returns BSTNode<T> the BSTNode whose data == data or null if no node is found
	 */
	public RBNode<K,V> search(K key){
		
		//use searchHelper method on root and data to allow for a recursive search of the tree
		return searchHelper(this.root, key);
		
	}
	
	/*
	 * recursive search of binary search tree to find node whose data == data
	 * @param data the data to be found in the binary search tree
	 * @returns BSTNode<T> the BSTNode whose data == data or null if no node is found
	 */
	private RBNode<K,V> searchHelper(RBNode<K,V> x, K key){
		
		//test special cases
		if (x == null || x.key.equals(key)){
			return x;
		}
		
		//if data < x.data, go left in tree
		if (key.compareTo(x.key) < 0){
			return searchHelper(x.left, key);
		}
			
		//else go right in tree
		else {return searchHelper(x.right, key);}
	}
}
