package edu.uab.cis.cs303.l3li3l.lab13;

import java.util.LinkedList;

public class PrimsMST {

	private double weight;       // total weight of MST
	private LinkedList<Edge> mst;     // edges in the MST
	private boolean[] marked;    // marked[v] = true if v on tree
	private PriorityQ<Edge> pq;      // edges with one endpoint in tree
	private WeightedGraph G;

	/**
	 * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
	 * @param G the edge-weighted graph
	 */
	public PrimsMST(WeightedGraph G, Integer d) {
		mst = new LinkedList<>();
		pq = new PriorityQ<>();
		marked = new boolean[G.V];
		this.G = G;
		
		if(d == Integer.MAX_VALUE){
			for (int v = 0; v < G.V; v++)     // run Prim from all vertices to
				if (!marked[v]) lazyPrim(v);     // get a minimum spanning forest
		}
		else {
			assert d < G.V && d >= 0; 
			
			danPrim(d);
		}
	}


	private void lazyPrim(int s) {
		scan(s);
		while (!pq.isEmpty()) {                        // better to stop when mst has V-1 edges
			Edge e = pq.pop();                      // smallest edge on pq
			int v = e.v, w = e.w;        // two endpoints
			assert marked[v] || marked[w];
			if (marked[v] && marked[w]) continue;      // lazy, both v and w already scanned
			mst.add(e);                            // add e to MST
			weight += e.weight;
			if (!marked[v]) scan(v);               // v becomes part of tree
			if (!marked[w]) scan(w);               // w becomes part of tree
		}
	}

	// add all edges e incident to v onto pq if the other endpoint has not yet been scanned
	private void scan(int v) {
		assert !marked[v];
		marked[v] = true;
		for (Edge e : G.adjw[v])
			if (!marked[e.w]) pq.add(e);
	}

	/**
	 * Returns the edges in a minimum spanning tree (or forest).
	 * @return the edges in a minimum spanning tree (or forest) as
	 *    an iterable of edges
	 */
	public Iterable<Edge> edges() {
		return mst;
	}

	/**
	 * Returns the sum of the edge weights in a minimum spanning tree (or forest).
	 * @return the sum of the edge weights in a minimum spanning tree (or forest)
	 */
	public double weight() {
		return weight;
	}
	
	public WeightedGraph getPrimmedGraph() {
		WeightedGraph g = new WeightedGraph(G.V, mst);
		return g;
	}

}
