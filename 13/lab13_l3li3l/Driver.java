package edu.uab.cis.cs303.l3li3l.lab13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		testTinyDG(); //Directed
		//testMediumDG();
		//testLargeDG();
		//testXtraLargeDG();
	}
	public static void testTinyDG() {
		FileReader r;
		try {
			System.out.println("\ntestTinyDG\n");
			r = new FileReader(new File("src/tinyDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			WeightedGraph g = new WeightedGraph(bf);
			System.out.println(g.tostring());
			//Initial dfs section
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void testMediumDG() {
		FileReader r;
		try {
			System.out.println("\nmediumDG\n");
			r = new FileReader(new File("src/mediumDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			WeightedGraph g = new WeightedGraph(bf);
			System.out.println(g.tostring());
			//Initial dfs section
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void testLargeDG() {
		FileReader r;
		try {
			System.out.println("\ntestLargeDG\n");
			r = new FileReader(new File("src/largeDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			WeightedGraph g = new WeightedGraph(bf);
			System.out.println(g.tostring());
			//Initial dfs section
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void testXtraLargeDG() {
		FileReader r;
		try {
			System.out.println("\ntestXtraLargeDG\n");
			r = new FileReader(new File("src/XtraLargeDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			WeightedGraph g = new WeightedGraph(bf);
			System.out.println(g.tostring());
			//Initial dfs section
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
