package edu.uab.cis.cs303.l3li3l.lab13;

import java.util.Stack;

/**
 * Implementation of Dijkstra's Algorithm on a Weighted Graph
 * @author Daniel Latham
 *
 */
public class DijkstrasAlgorithm {
	
	private double[] distTo;          // distTo[v] = distance  of shortest s->v path
	private Edge[] edgeTo;    // edgeTo[v] = last edge on shortest s->v path
	private IndexMinPQ<Double> pq;    // priority queue of vertices

	public DijkstrasAlgorithm(WeightedGraph G, int s) {

		distTo = new double[G.V];
		edgeTo = new Edge[G.V];
		//equivalent to setting all distances to far, far away//not been passed
		//INITIALIZE-SINGLE-SOURCE
		for (int v = 0; v < G.V; v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		//this node's distance is always 0
		distTo[s] = 0.0;

		// relax vertices in order of distance from s
		//initialize PriorityQueue
		pq = new IndexMinPQ<Double>(G.V);
		//initial values of source
		pq.insert(s, distTo[s]);
		while (!pq.isEmpty()) {
			//deletes source first, then goes through edges adjacent to it
			int v = pq.delMin();
			for (Edge e : G.adjw[v])
				relax(e);
		}

	}

	// relax edge e and update pq if changed
	private void relax(Edge e) {
		int v = e.v, w = e.w;
		if (distTo[w] > distTo[v] + e.weight) {
			distTo[w] = distTo[v] + e.weight;
			edgeTo[w] = e;
			if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
			else                pq.insert(w, distTo[w]);
		}
	}


	public double distTo(int v) {
		return distTo[v];
	}


	public boolean hasPathTo(int v) {
		return distTo[v] < Double.POSITIVE_INFINITY;
	}


	public Stack<Edge> pathTo(int v) {
		if (!hasPathTo(v)) return null;
		Stack<Edge> path = new Stack<>();
		for (Edge e = edgeTo[v]; e != null; e = edgeTo[e.v]) {
			path.push(e);
		}
		return path;
	}

}
