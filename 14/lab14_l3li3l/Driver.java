package edu.uab.cis.cs303.l3li3l.lab13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Stack;

/**
 * Driver for the WeightedGraph and Dijkstra's Algorithm
 * This driver tests all methods on newly created classes.
 * 
 * @author Daniel Latham
 *
 */
public class Driver {
	
	public static void main(String[] args) {
		
		testTinyDG();
		testMediumDG();
		testLargeDG();
		testXtraLargeDG();
	}
	public static void testTinyDG() {
		FileReader r;
		try {
			System.out.println("\ntestTinyDG\n");
			r = new FileReader(new File("src/tinyDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			WeightedGraph g = new WeightedGraph(bf);
			
			long time = System.nanoTime();
			DijkstrasAlgorithm da = new DijkstrasAlgorithm(g, 0);
			time = System.nanoTime()-time;
			time = time / 1000; //Microseconds
			
			System.out.println("Time taken: "+time+" Microseconds\n");
			
			for(int i = 0; i < g.V; i++){
				double dist = da.distTo(i);
				Stack<Edge> z = da.pathTo(i);
				String s = "";
				s += i + ": " + dist + " | ";
				for(int j = z.size()-1; j >= 0; j--){
					s += z.get(j).v + " -> ";
				}
				s += i;
				System.out.println(s);
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void testMediumDG() {
		FileReader r;
		try {
			System.out.println("\ntestMediumyDG\n");
			r = new FileReader(new File("src/mediumDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			WeightedGraph g = new WeightedGraph(bf);
			
			long time = System.nanoTime();
			DijkstrasAlgorithm da = new DijkstrasAlgorithm(g, 0);
			time = System.nanoTime()-time;
			time = time / 1000; //Microseconds
			
			System.out.println("Time taken: "+time+" Microseconds\n");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void testLargeDG() {
		FileReader r;
		try {
			System.out.println("\ntestLargeDG\n");
			r = new FileReader(new File("src/largeDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			WeightedGraph g = new WeightedGraph(bf);
			
			long time = System.nanoTime();
			DijkstrasAlgorithm da = new DijkstrasAlgorithm(g, 0);
			time = System.nanoTime()-time;
			time = time / 1000; //Microseconds
			
			System.out.println("Time taken: "+time+" Microseconds\n");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void testXtraLargeDG() {
		FileReader r;
		try {
			System.out.println("\ntestXtraLargeDG\n");
			r = new FileReader(new File("src/XtraLargeDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			WeightedGraph g = new WeightedGraph(bf);
			
			long time = System.nanoTime();
			DijkstrasAlgorithm da = new DijkstrasAlgorithm(g, 0);
			time = System.nanoTime()-time;
			time = time / 1000; //Microseconds
			
			System.out.println("Time taken: "+time+" Microseconds\n");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
