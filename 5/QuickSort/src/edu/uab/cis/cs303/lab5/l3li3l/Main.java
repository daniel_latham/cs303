package edu.uab.cis.cs303.lab5.l3li3l;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import src.com.*;
import src.com.csvreader.CsvWriter;

/**
 * @author Daniel Latham
 */
public class Main {

	/**
	 */
	private static void importVars(String one)
	{
		
		File f = null;
		try {
			f = new File(one);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		Scanner s = null;
		try {
			s = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		ArrayList<Integer> all = new ArrayList<>();
		int i = 0;
		//Adds all elements in the file to the arraylist
		while (s.hasNextInt() && (i < Math.pow(2, 10))) {
			all.add(s.nextInt());
			i++;
		} 
		
		
		
		//Print file name
		System.out.println("File: "+one);
		System.out.println("Array length: " + all.size());
		//Tests search algorithm then exits
		long timea0 = alg.testHeapSort(all);
		long timeb0 = alg.testMergeSort(all);
		long timec0 = alg.testInsertionSort(all);
		long timea1 = alg.testHeapSort(all);
		long timeb1 = alg.testMergeSort(all);
		long timec1 = alg.testInsertionSort(all);
		long timea2 = alg.testHeapSort(all);
		long timeb2 = alg.testMergeSort(all);
		long timec2 = alg.testInsertionSort(all);
		long timed0 = alg.testQuickSort(all);
		long timed1 = alg.testQuickSort(all);
		long timed2 = alg.testQuickSort(all);
		long timee0 = alg.testQuickSortP3(all);
		long timee1 = alg.testQuickSortP3(all);
		long timee2 = alg.testQuickSortP3(all);
		//take averages of 3 tests
		long timea = (timea0+timea1+timea2)/3;
		long timeb = (timeb0+timeb1+timeb2)/3;
		long timec = (timec0+timec1+timec2)/3;
		long timed = (timed0+timed1+timed2)/3;
		long timee = (timee0+timee1+timee2)/3;
		//add to arrays CSV
		col1.add(one.substring(one.lastIndexOf("/")+1,one.lastIndexOf(".")));
		col2.add(Long.toString(timea));
		col3.add(Long.toString(timeb));
		col4.add(Long.toString(timec));
		col5.add(Long.toString(timed));
		col6.add(Long.toString(timee));
		//Print output
		System.out.println("Test: HeapSort, Time: " + timea + " microseconds");
		System.out.println("Test: MergeSort, Time: " + timeb + " microseconds");
		System.out.println("Test: InsertionSort, Time: " + timec + " microseconds");
		System.out.println("Test: QuickSort, Time: " + timed + " microseconds");
		System.out.println("Test: QuickSortP3, Time: " + timee + " microseconds");
		
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		assert args.length >= 1: "Expected 1 or more input";
		//implied that args is in order number, path, path, path... etc
		//setup CSV
		col1.add("Name");
		col2.add("HeapSort");
		col3.add("MergeSort");
		col4.add("InsertionSort");
		col5.add("QuickSort");
		col6.add("QuickSortP3");
		//
		for (int i = 0; i < args.length; i++) {
			if(args[i].endsWith("INPUTS.txt")) {
				File f = null;
				Scanner s = null;
				try {
					f = new File(args[0]);
					s = new Scanner(f);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				while (s.hasNext()) {
					importVars(s.next());
				}
			}
			else {
				importVars(args[i]);
			}
		}
		//Add lists to CSV file
		String[] a = new String[col1.size()];
		String[] b = new String[col2.size()];
		String[] c = new String[col3.size()];
		String[] d = new String[col4.size()];
		String[] e = new String[col5.size()];
		String[] f = new String[col6.size()];
		try {
			csvw.writeRecord(col1.toArray(a));
			csvw.writeRecord(col2.toArray(b));
			csvw.writeRecord(col3.toArray(c));
			csvw.writeRecord(col4.toArray(d));
			csvw.writeRecord(col5.toArray(e));
			csvw.writeRecord(col6.toArray(f));
		} catch (IOException ef) {
			// TODO Auto-generated catch block
			ef.printStackTrace();
		}
		try {
			csvw.endRecord();
		} catch (IOException ee) {
			// TODO Auto-generated catch block
			ee.printStackTrace();
		}
		csvw.close();
	} 

	private final static AlgorithmDriver alg = new AlgorithmDriver();
	private final static CsvWriter csvw = new CsvWriter("CSV.csv");
	private static ArrayList<String> col1 = new ArrayList<>();
	private static ArrayList<String> col2 = new ArrayList<>();
	private static ArrayList<String> col3 = new ArrayList<>();
	private static ArrayList<String> col4 = new ArrayList<>();
	private static ArrayList<String> col5 = new ArrayList<>();
	private static ArrayList<String> col6 = new ArrayList<>();
}

