package edu.uab.cis.cs303.lab5.l3li3l;

import java.util.ArrayList;

public class QuickSort {
	
	
	public ArrayList<Integer> sort(ArrayList<Integer> A, Integer p, Integer r) {
		if (p < r) {
			Integer q = partition(A, p, r);
			A = sort(A, p, q-1);
			A = sort(A, q+1, r);
		}
		return A;
	}
	
	protected Integer partition(ArrayList<Integer> A, Integer p, Integer r) {
		Integer x = A.get(r);
		Integer i = p-1;
		for (int j = p; j < r; j++) {
			if (A.get(j) <= x) {
				i = i+1;
				Integer temp = A.get(i);
				A.set(i, A.get(j));
				A.set(j, temp);
			}
		}
		Integer temp = A.get(i+1);
		A.set(i+1, A.get(r));
		A.set(r, temp);
		return i+1;
	}

}
