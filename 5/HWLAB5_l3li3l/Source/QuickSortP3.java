package edu.uab.cis.cs303.lab5.l3li3l;

import java.util.ArrayList;

public class QuickSortP3 extends QuickSort {

	ArrayList<Integer> data = null;
	Integer len = null;
	
	public ArrayList<Integer> get(){
		return data;
	}
	
	public void quickSort(ArrayList<Integer> data) {
		this.data = data;
		this.len = data.size();
		recQuickSort(0, len - 1);
	}

	public void recQuickSort(int left, int right) {
		int size = right - left + 1;
		if (size <= 3) // manual sort if small
			insertionSort(left, right);
		else // quicksort if large
		{
			int median = medianOf3(left, right);
			int partition = partitionIt(left, right, median);
			recQuickSort(left, partition - 1);
			recQuickSort(partition + 1, right);
		}
	}

	public int medianOf3(int left, int right) {
		int center = (left + right) / 2;
		// order left & center
		if (data.get(left) > data.get(center))
			swap(left, center);
		// order left & right
		if (data.get(left) > data.get(right))
			swap(left, right);
		// order center & right
		if (data.get(center) > data.get(right))
			swap(center, right);

		swap(center, right - 1); // put pivot on right
		return data.get(right - 1); // return median value
	}

	public void swap(int dex1, int dex2) {
		int temp = data.get(dex1);
		data.set(dex1, data.get(dex2));
		data.set(dex2,temp);
	}

	public int partitionIt(int left, int right, long pivot) {
		int leftPtr = left; // right of first elem
		int rightPtr = right - 1; // left of pivot

		while (true) {
			//       find bigger
			while (data.get(leftPtr) < pivot) leftPtr++;
			//       find smaller
			while (data.get(rightPtr) > pivot) rightPtr--;
			
			if (leftPtr >= rightPtr){
				// if pointers cross, partition done
				break;
			}
			// not crossed, so
			swap(leftPtr, rightPtr); // swap elements
		}
		swap(leftPtr, right - 1); // restore pivot
		return leftPtr; // return pivot location
	}

	public void insertionSort(Integer p, Integer r) {
		for (int j = p; j <= r; j++)
		{
			Integer key = data.get(j);
			// Insert l[j] into the sorted sequnce l[1..j-1]
			Integer i = j-1;
			while (i >= 0 && data.get(i) > key) {
				data.set(i+1, data.get(i));
				i = i -1;
			}
			data.set(i+1, key);
		}
	}

}
