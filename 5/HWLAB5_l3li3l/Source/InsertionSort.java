package edu.uab.cis.cs303.lab5.l3li3l;

import java.util.ArrayList;

public class InsertionSort {
	public ArrayList<Integer> sort(ArrayList<Integer> l) {
		for (int j = 1; j < l.size(); j++)
		{
			Integer key = l.get(j);
			// Insert l[j] into the sorted sequnce l[1..j-1]
			Integer i = j-1;
			while (i >= 0 && l.get(i) > key) {
				l.set(i+1, l.get(i));
				i = i -1;
			}
			l.set(i+1, key);
		}
		return l;
	}
}
