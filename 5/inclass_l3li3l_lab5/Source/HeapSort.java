package edu.uab.cis.cs303.lab5.l3li3l;

import java.util.ArrayList;

public class HeapSort {

	private Integer heapsize = 0;
	public ArrayList<Integer> sort(ArrayList<Integer> A) {
		A = buildMaxHeap(A);
		for (int i = A.size()-1; i >= 1; i--) {
			Integer temp = A.get(0);
			A.set(0, A.get(i));
			A.set(i, temp);
			heapsize -= 1;
			A = maxHeapify(A, 0);
		}
		return A;
	}
	public ArrayList<Integer> buildMaxHeap(ArrayList<Integer> A) {
		heapsize = A.size()-1;
		for (int i = A.size()/2; i >= 0; i--) {
			A = maxHeapify(A, i);
		}
		return A;
	}
	
	public ArrayList<Integer> maxHeapify(ArrayList<Integer> A, Integer i) {
		Integer l = 2*i+1;
		Integer r = 2*i+2;
		Integer largest = 0;
		if (l <= heapsize && A.get(l) > A.get(i)) {
			largest = l;
		} else { 
			largest = i;
		}
		if (r <= heapsize && A.get(r) > A.get(largest)) {
			largest = r;
		}
		if (largest != i) {
			Integer temp = A.get(i);
			A.set(i, A.get(largest));
			A.set(largest, temp);
			A = maxHeapify(A, largest);
		}
		return A;
	}
	
}
