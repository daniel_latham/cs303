# -*- coding: utf-8 -*-
"""
LinkedList implementation for CS303
Created on Wed Feb 11 12:23:35 2015

@author: Daniel Latham
@email: l3li3l@uab.edu
"""
class Node(object):  
    def __init__(self, data):
        self.data = data
        self.nextNode = None


class LinkedList(object):
    
    def __init__(self, head=None):
        self.head = head
        
    def size(self):
        if not self.isEmpty():
            i = 1
            nextN = self.head.nextNode
            while nextN is not None:
                i+=1
                nextN = nextN.nextNode       
            return i
        else: 
            return 0
    
    def isEmpty(self):
        if self.head == None:
            return True
        else:
            return False
    
    def get(self, i):
        if not self.isEmpty() and not i < 0 and not i >= self.size():
            j = 0
            nextN = self.head
            while nextN is not None and j is not i:
                j+=1
                nextN = nextN.nextNode       
            return nextN.data
        else: 
            return -1
    
    def add(self, x):
        if self.head == None:
            self.head = x
        else:
            nextN = self.head
            while nextN is not None and nextN.nextNode is not None:
                nextN = nextN.nextNode
                #print nextN.data
            nextN.nextNode = x
    
    def remove(self):
        if self.isEmpty():
            return
        else:
            nextN = self.head
            while nextN.nextNode is not None:
                prevN = nextN
                #print nextN.data
                nextN = nextN.nextNode
                #print nextN.data
            prevN.nextNode = None
    
    def contains(self, val):
        if not self.isEmpty():
            nextN = self.head
            j = 0
            while nextN is not None:
                j+=1
                nextN = nextN.nextNode       
                if nextN.data == val:
                    return j
            return -1
        else: 
            return -1
    
    def addI(self, x, index):
        assert index <= self.size()
        if self.isEmpty():
            return
        elif index == 0:
            temp = self.head
            self.head = x
            x.nextNode = temp
        else:
            j = 0
            thisN = self.head
            while (j < index):
                prevN = thisN
                thisN = thisN.nextNode
                j+=1
            prevN.nextNode = x
            x.nextNode = thisN
            
    
    def removeI(self, index):
        assert index <= self.size()
        if self.isEmpty():
            return
        elif index == 0:
            self.head = self.head.nextNode
        else:
            j = 0
            thisN = self.head
            while (j < index):
                prevN = thisN
                thisN = thisN.nextNode
                j+=1
            prevN.nextNode = thisN.nextNode
     

class Driver(object):
    def __init__(self):
        print "Initialize new list"
        ln = LinkedList()
        print 
        print "Is this empty(Should be True)"
        print ln.isEmpty()
        print "Is the size correct?(Should be 0)"
        print ln.size()
        print 
        print "Add numbers with add() method 0, 1, 2, 3, 4, 5, 6, 7, 8"
        for i in xrange(0,9):
            ln.add(Node(i))
        print "New linked list retried with get() method: "
        print "[",
        for i in xrange(0,9):
            print ln.get(i),",",
        print "]"
        print
        print "Is this empty(Should be False)"
        print ln.isEmpty()
        print "Is the size correct?(Should be 9)"
        print ln.size()
        
        print
        print "Remove at index 0 and end with remove() and removeI()"
        ln.remove()
        ln.removeI(0)
        print "Linked list printed again: "
        print "[",
        for i in xrange(0,7):
            print ln.get(i),",",
        print "]"
        print 
        print "Add 5999 to index 4"
        ln.addI(Node(5999),4)
        print
        print "Linked list printed again: "
        print "[",
        for i in xrange(0,8):
            print ln.get(i),",",
        print "]"
        
        print
        print "Tests contains: "
        print "Linked list contains 5999 at index: ",
        print ln.contains(5999)
        
        
        
        
        
        
d = Driver()