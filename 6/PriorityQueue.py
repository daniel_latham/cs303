# -*- coding: utf-8 -*-
"""
SYNOPSIS

    Basic implementation of a priority queue in Python2.7
    Python2.7 is installed by default on most platforms
    Unix based OS's may check version of python with command:
        python -V

DESCRIPTION

    Priority Queue data-structure that works using an underlying heap.Gives highest priority to the minimum element
    of an array and uses a max heap to form this priority queue. Methods insert() and delete() have the following
    signatures:
        int insert( T a) :
            Takes as input “a” of type T and returns the index in the array where the element is inserted.

        Boolean delete(T a) :
            Takes as input “a” of type T and true if the element is found and is deleted and returns false when either
            the number is not found for deletion or the deletion was not successful.


EXAMPLES

    python PriorityQueue.py

EXIT STATUS

    Code 0

AUTHOR

    Daniel Latham <l3li3l@uab.edu>

LICENSE

    This script is in the public domain, free from copyrights or restrictions.

VERSION

    1.0
"""

__author__ = 'Daniel Latham'

class PriorityQueue(object):

    def __init__(self, heap=[]):
        self.heap = heap

        #Prepend NONE element to heap
        self.heap = self.heap[::-1]
        self.heap.append(None)
        self.heap = self.heap[::-1]


        ##Finally heapify implicitly declaring min value highest priority
        self.__build_max_heap()

    ###Inserts value at end of heap, re-heapify
    ###returns i such that i = heap.getPlace(value)
    ###If there are duplicates in the list
    def insert(self, item):
        self.heap.append(item)
        self.__build_max_heap()
        return self.heap.index(item)

    ###Because this is a queue, it will delete ALL elements until it finds item
    ###If duplicates exist in the list, it will delete the first found value
    ###Brutal dude, be careful
    ###Was this asked of us because it is a horrible, terrible implementation?
    def delete(self, item):
        place = 1
        num = self.heap[place]


        while(num != item):
            if len(self.heap) == 1:
                return False
            #switch last and first elements
            temp = self.heap[place]
            self.heap[place] = self.heap[len(self.heap)-1]
            self.heap[len(self.heap)-1] = temp
            #pop last element
            num = self.heap.pop()
            #heapify
            self.__build_max_heap()
        return True


    def __max_heapify(self, i):
        left = 2 * i
        right = 2 * i +1
        smallest = i
        if left < len(self.heap) and self.heap[left] < self.heap[smallest]:
            smallest = left
        if right < len(self.heap) and self.heap[right] < self.heap[smallest]:
            smallest = right
        if smallest != i:
            self.heap[i], self.heap[smallest] = self.heap[smallest], self.heap[i]
            self.__max_heapify(smallest)

    def __build_max_heap(self):
        for i in range((len(self.heap) // 2)+1, 0, -1):
            self.__max_heapify(i)


def driver():
    pq = PriorityQueue([5, 5, 3, 2, 5, 6, 7, 8, 13, 9, 2, 3]);

    print "PriorityQueue == " + str(pq.heap)
    print
    print "Inserted value 99 at place " + str(pq.insert(99))
    print
    print "PriorityQueue == " + str(pq.heap)
    print
    print "Inserted value 1 at place " + str(pq.insert(1))
    print
    print "PriorityQueue == " + str(pq.heap)
    print
    print "Removal of value 6 from PriorityQueue successful?: " + str(pq.delete(6))
    print
    print "PriorityQueue == " + str(pq.heap)
    print
    print "Removal of value 77 from PriorityQueue successful?: " + str(pq.delete(77))
    print
    print "PriorityQueue == " + str(pq.heap)

def __main__():
    driver()

if __name__ == "__main__":
    __main__()