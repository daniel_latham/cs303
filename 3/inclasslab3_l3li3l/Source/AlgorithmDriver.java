package edu.uab.cis.l3li3l.lab3;

import java.util.ArrayList;

public class AlgorithmDriver {
		public long testSort(ArrayList<Integer> l){
			MergeSort ll = new MergeSort();
			long beg = System.nanoTime();
			ArrayList<Integer> ret = ll.sort(l);
			long end = System.nanoTime();
			long time = ((end-beg)/1000);
			System.out.println("Array length: " + ret.size());
			System.out.println(ret.toString());
			return time;
		}
}
