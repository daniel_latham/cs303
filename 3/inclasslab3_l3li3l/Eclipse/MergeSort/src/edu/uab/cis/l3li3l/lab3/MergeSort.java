package edu.uab.cis.l3li3l.lab3;

import java.util.ArrayList;

public class MergeSort {
	/**
	 * Recursively sorts elements in an array by dividing into equally sized sublists, eventually 
	 * becoming  arrays of size 1, then merging 
	 * @param m ArrayList to sort
	 * @return sorted ArrayList of integers
	 */
	public ArrayList<Integer> sort(ArrayList<Integer> m)
	{
		// Base case. A list of zero or one elements is sorted, by definition.
		if (m.size() <= 1)
		{
			return m;
		}
		// Recursive case. First, *divide* the list into equal-sized sublists.
		ArrayList<Integer> left, right;
		left = new ArrayList<>();
		right = new ArrayList<>();
		Integer middle = m.size() / 2;
		for (int i = 0; i < middle; i++)
		{
			left.add(m.get(i));
		}
		for (int i = middle; i < m.size(); i++)
		{
			right.add(m.get(i));
		}
		// Recursively sort both sublists.
		left = sort(left);
		right = sort(right);
		// *Conquer*: merge the now-sorted sublists.
		return merge(left, right);
	}
	/**
	 * Merges two sublists of integers
	 * @param left sublist on the left side
	 * @param right sublist on the right side
	 * @return ArrayList result of merging two sublists in ascending order
	 */
	private ArrayList<Integer> merge(ArrayList<Integer> left, ArrayList<Integer> right){
		ArrayList<Integer> result = new ArrayList<>();
		//merge elements into result in ascending order
		while (!left.isEmpty() && !right.isEmpty())
		{
			if (left.get(0) <= right.get(0))
			{
				result.add(left.get(0));
				left.remove(0);
			}
			else {
				result.add(right.get(0));
				right.remove(0);
			}
		}
		// either left or right may have elements left
		while (!left.isEmpty())
		{
			result.add(left.get(0));
			left.remove(0);
		}
		while (!right.isEmpty()){
			result.add(right.get(0));
			right.remove(0);
		}
		return result;
	}
}
