package edu.uab.cis.cs303.l3li3l.lab12;

import java.util.Stack;

public class DepthFirstPath {
	private boolean[] marked;	// marked[v] = is there an s-v path?
	private int[] edgeTo;		// edgeTo[v] = last edge on s-v path
	private final int s;		//source vertex

	/**
	 * Constructor, initializes edgeTo and marked
	 * @param G Graph object
	 * @param s integer to start search from
	 */
	public DepthFirstPath(Graph G, int s) {
		this.s = s;
		this.edgeTo = new int[G.V];
		this.marked = new boolean[G.V];
		dfs(G, s); //star depth first search
	}

	/**
	 * Depth first search implementation
	 * @param G Graph object
	 * @param v int to start search from
	 */
	private void dfs(Graph G, int v) {
		
		marked[v] = true; //this vertex has been visited
		
		//iterate through all vertices in adjacency list
		//if they haven't been visited(marked[w] == false)
		for (int w : G.adj[v]) {
			if (!marked[w]) {
				edgeTo[w] = v; //parent of w is v
				dfs(G, w); //recurse from w
			}
		}
	}


	/**
	 * Returns true if there is a path(has been visited) to vertex v
	 * @param v int value representing vertex
	 * @return boolean if/been visited
	 */
	public boolean hasPathTo(int v) {
		return marked[v];
	}

	/**
	 * Generates a stack of all vertices to vertex v from s
	 * @param v int representing vertex
	 * @return Iterable<Integer> representing path from s(source) to v(exit)
	 */
	public Iterable<Integer> pathTo(int v) {
		//checks if v has been visited, returns null if not
		if (!hasPathTo(v)){
			return null;
		}
		//instantiates stack that represents the path from s to v
		Stack<Integer> path = new Stack<Integer>();
		//start at v, keep going until find s, update x to parent
		for (int x = v; x != s; x = edgeTo[x])
			//push x to stack
			path.push(x);
		//finally push s to stack
		path.push(s);
		//stack in for v ... s, to print path in proper order, reverse stack and print
		return path;
	}

}
