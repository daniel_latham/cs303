package edu.uab.cis.cs303.l3li3l.lab12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;

public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		testTinyDG(); //Directed
	}
	public static void testTinyDG() {
		FileReader r;
		try {
			System.out.println("\ntestTinyDG\n");
			r = new FileReader(new File("src/tinyDG.txt"));
			BufferedReader bf = new BufferedReader(r);
			DirectedGraph g = new DirectedGraph(bf);
			//Initial dfs section
			System.out.println("Before Topological Sort; dfs from node 0: ");
			int z = 0;
			DepthFirstPath dfp = new DepthFirstPath(g,z);
			//Base string for printing paths From 'x' to 'y'
			String xl = "From " + Integer.toString(z) + " to ";
			for (int i = 0; i < g.V; i++){
				String s = "";
				String lz = Integer.toString(i);
				if (i < 10) lz += " ";
				//returned stack of ints
				Iterable<Integer> kk = dfp.pathTo(i);
				if (kk == null) {
					System.out.println(xl + lz + ": NONE");
					
				} else {
					for (int k : kk){
						s =  Integer.toString(k)+", " + s;
					}
					
					s = xl + lz + ": " + s.substring(0, s.length() - 2);
					System.out.println(s);
				}
			}
			
			
			
			//Topological sort section
			LinkedList<Integer> l = dfp.topologicalSort();
			
			System.out.println("\nAfter Topological Sort; all nodes traversed:");
			xl = "From " + Integer.toString(z) + " to ";
			for (int i = 0; i < g.V; i++){
				String s = "";
				String lz = Integer.toString(i);
				if (i < 10) lz += " ";
				//returned stack of ints
				Iterable<Integer> kk = dfp.pathTo(i);
				if (kk == null) {
					System.out.println(xl + lz + ": NONE");
					
				} else {
					for (int k : kk){
						s =  Integer.toString(k)+", " + s;
					}
					
					s = xl + lz + ": " + s.substring(0, s.length() - 2);
					System.out.println(s);
				}
			}
			
			
			//Topological sort print
			System.out.println("\nTopological Sort in Order:");
			String s = "";
			int k = l.size();
			for (int i = 0; i < k; i++){
				s += (l.pop() + ", ");
			}
			System.out.println(s.subSequence(0, s.length()-2));

			
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}
