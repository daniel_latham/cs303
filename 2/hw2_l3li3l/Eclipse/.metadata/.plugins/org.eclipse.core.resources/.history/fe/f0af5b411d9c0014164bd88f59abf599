package edu.uab.cis.l3li3l.lab2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * To use this program, execute 
 * $: java -ea Main number /path/to/input.txt another/path another/path.. etc 
 * This program will import a list of integers and place them in an array in the same order
 * they are given for the Linear Search as well as a sorted array for the Binary Search and then test
 * the arrays with a given input number to search.
 * @author Daniel Latham
 */
public class Main {

	/**
	 * Imports variables and calls on tests
	 * @param one String representation of int that needs to be found
	 * @param two Path to input.txt
	 */
	private static void importVars(String one, String two)
	{
		File f = null;
		int k = 0;
		try {
			f = new File(two);
			k = Integer.parseInt(one);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Scanner s = null;
		try {
			s = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		ArrayList<Integer> all = new ArrayList<>();
		//Adds all elements in the file to the arraylist
		while (s.hasNextInt()) {
			all.add(s.nextInt());
		}
		
		ArrayList<Integer> allSorted = new ArrayList<>();
		//allSorted is a copy of all
		allSorted.addAll(all);
		//Sorts allSorted in ascending order
		Collections.sort(allSorted);
		
		//Instantiates two new arrays of ints to be used for tests
		int[] l = new int[all.size()];
		int[] lSorted = new int[allSorted.size()];
		
		//Adds all elements in arraylists to arrays
		for (int i = 0; i < all.size(); i++){
			l[i] = all.get(i);
			lSorted[i] = allSorted.get(i);
		}
		
		//Tests Linear and Binary search algorithm then exits
		ArrayList<Number> line = alg.testLinear(l, k);
		ArrayList<Number> bin = alg.testBinary(lSorted, k);
		//Print output
		System.out.println("Algorithm: LinearSearch" + ", File: " + two.substring(two.lastIndexOf("/")) + 
				", Number: " + one + ", Location: " + line.get(0) + ", Time: " + line.get(1) + " MicroSeconds");
		System.out.println("Algorithm: BinarySearch" + ", File: " + two.substring(two.lastIndexOf("/")) + 
				", Number: " + one + ", Location: " + bin.get(0) + ", Time: " + bin.get(1) + " MicroSeconds");
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		assert args.length >= 2: "Excpected 2 or more input";
		//implied that args is in order number, path, path, path... etc
		for (int i = 1; i < args.length; i++) {
			importVars(args[0], args[i]);
		}
	} 

	private final static AlgorithmDriver alg = new AlgorithmDriver();
}

