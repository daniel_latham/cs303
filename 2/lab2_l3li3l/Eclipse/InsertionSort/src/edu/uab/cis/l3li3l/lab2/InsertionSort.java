package edu.uab.cis.l3li3l.lab2;

public class InsertionSort {

	public int[] sort(int[] l) {
		for (int j = 1; j < l.length; j++)
		{
			int key = l[j];
			// Insert l[j] into the sorted sequnce l[1..j-1]
			int i = j-1;
			while (i >= 0 && l[i] > key) {
				l[i+1] = l[i];
				i = i -1;
			}
			l[i+1] = key;
		}
		return l;
	}
}
