package edu.uab.cis.l3li3l.lab2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Daniel Latham
 */
public class Main {

	/**
	 */
	private static void importVars(String one)
	{
		File f = null;
		try {
			f = new File(one);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Scanner s = null;
		try {
			s = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		ArrayList<Integer> all = new ArrayList<>();
		//Adds all elements in the file to the arraylist
		while (s.hasNextInt()) {
			all.add(s.nextInt());
		}
		
		
		//Instantiates new integer array
		int[] l = new int[all.size()];
		
		//Adds all elements in arraylists to arrays
		for (int i = 0; i < all.size(); i++){
			l[i] = all.get(i);
		}
		
		//Tests Linear and Binary search algorithm then exits
		long time = alg.testSort(l);
		//Print output
		System.out.println("Time: " + time + " microseconds");
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		assert args.length >= 1: "Excpected 1 or more input";
		//implied that args is in order number, path, path, path... etc
		for (int i = 0; i < args.length; i++) {
			importVars(args[i]);
		}
	} 

	private final static AlgorithmDriver alg = new AlgorithmDriver();
}

