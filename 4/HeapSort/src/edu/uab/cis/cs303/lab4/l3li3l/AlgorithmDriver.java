package edu.uab.cis.cs303.lab4.l3li3l;

import java.util.ArrayList;

public class AlgorithmDriver {
		public long testHeapSort(ArrayList<Integer> l){
			HeapSort ll = new HeapSort();
			long beg = System.nanoTime();
			ArrayList<Integer> ret = ll.sort(l);
			long end = System.nanoTime();
			long time = ((end-beg)/1000);
			//System.out.println(ret.toString());
			return time;
		}
		public long testMergeSort(ArrayList<Integer> l){
			MergeSort ll = new MergeSort();
			long beg = System.nanoTime();
			ArrayList<Integer> ret = ll.sort(l);
			long end = System.nanoTime();
			long time = ((end-beg)/1000);
			//System.out.println(ret.toString());
			return time;
		}
		
		public long testInsertionSort(ArrayList<Integer> l) {
			InsertionSort ll = new InsertionSort();
			long beg = System.nanoTime();
			ArrayList<Integer> ret = ll.sort(l);
			long end = System.nanoTime();
			long time = ((end-beg)/1000);
			//System.out.println(ret.toString());
			return time;
		}
}
