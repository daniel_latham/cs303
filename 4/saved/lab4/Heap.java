package lab4;

public class Heap {
	
	public static int n;
	
	public static void heapsort(int[] A){
		
		n = A.length - 1;
		buildMaxHeap(A);
		
		for (int i = A.length - 1; i >= 1; i--){
			
			exchange(A, 0, i);
			n--;
			maxHeapify(A,0);
			
		}
		
	}
	
	public static void buildMaxHeap(int[] A){
		
		for (int i = n/2; i >= 0; i--){
			 maxHeapify(A, i);
			 
		}
	}
	
	public static void maxHeapify(int[] A, int i){
		
		int largest;
		int l = i * 2 + 1;
		int r = i * 2 + 2;
		
		if ((l <= n) && (A[l] > A[i])){
			largest = l;
		}
		else largest = i;
		if ((r <= n) && (A[r] > A[largest])){
			largest = r;
		}
		if (largest != i){
			exchange(A, largest, i);
			maxHeapify(A, largest);
		}
	}
	
	public static void exchange(int[] A, int e, int x){
		
		int tmp = A[e];
		A[e] = A[x];
		A[x] = tmp;
		
	}
}
