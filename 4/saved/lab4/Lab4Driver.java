package lab4;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Lab4Driver {


	static int[] arr;//used to hold each set of arrays
	
	public static void main(String[] args){
		
		FileInputStream fis = null;
		
		try{
			File file = new File("/Users/amalee/Downloads/folder-3/input_32.txt");
			
				//read in file to arrays, modeled after fileRead() in FileReadSortCheck
				byte[] bytes = new byte[(int) file.length()];
				fis = new FileInputStream(file);
				fis.read(bytes);
				fis.close();
				String[] valueStr = new String(bytes).trim().split("\\s+");
				arr = new int[valueStr.length];
				for (int i = 0; i < valueStr.length; i++) {
						arr[i] = Integer.parseInt(valueStr[i]);
				}
				
				Heap.heapsort(arr);
				for (int i = 0; i < valueStr.length; i++) {
					System.out.println(arr[i]);
			}
			
				System.out.println(FileReadSortCheck.sortCheckAsc(arr));
				
			}
        	catch (IOException e) {//catches exceptions
                e.printStackTrace();
            } finally {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
	}
}
