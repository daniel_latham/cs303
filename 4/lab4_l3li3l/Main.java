package edu.uab.cis.cs303.lab4.l3li3l;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Daniel Latham
 */
public class Main {

	/**
	 */
	private static void importVars(String one)
	{
		File f = null;
		try {
			f = new File(one);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Scanner s = null;
		try {
			s = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		ArrayList<Integer> all = new ArrayList<>();
		//Adds all elements in the file to the arraylist
		while (s.hasNextInt()) {
			all.add(s.nextInt());
		}
		
		
		//Print file name
		System.out.println("File: "+one);
		System.out.println("Array length: " + all.size());
		//Tests Linear and Binary search algorithm then exits
		long time = alg.testHeapSort(all);
		//Print output
		System.out.println("Test: HeapSort, Time: " + time + " microseconds");
		
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		assert args.length >= 1: "Expected 1 or more input";
		//implied that args is in order number, path, path, path... etc
		for (int i = 0; i < args.length; i++) {
			if(args[i].endsWith("INPUTS.txt")) {
				File f = null;
				Scanner s = null;
				try {
					f = new File(args[0]);
					s = new Scanner(f);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				while (s.hasNext()) {
					importVars(s.next());
				}
			}
			else {
				importVars(args[i]);
			}
		}
	} 

	private final static AlgorithmDriver alg = new AlgorithmDriver();
}

