# CS303 Algorithms and Data Structures

Course homework and lab assignments(except for the lab reports) for CS303 Algorithms and Data Structures at UAB. This is intended as a guide for any future students at UAB or elsewhere. DO NOT plagiarize, please!

### Details

* Java 8 implementations 
* Python 2.7 implementations
* Blood, sweat, tears
