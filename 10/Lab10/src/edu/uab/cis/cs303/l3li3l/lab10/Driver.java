package edu.uab.cis.cs303.l3li3l.lab10;

import java.io.BufferedReader;
import java.io.FileReader;

public class Driver {

	public static void main(String[] args){
		System.out.println("given");
		given();
		System.out.println();
		//System.out.println("linear");
		//linear();
		System.out.println();
		//System.out.println("quadratic");
		//quadratic();
		System.out.println();
		//System.out.println("redBlack");
		//redBlack();
	}
	
	public static void given(){
		HashMap<String> t = new HashMap<>(100000000);
		
		BufferedReader br = null;
		String line = "";
		String split = ",";//used to separate by comma
		String[][] table = new String[16][];//table for output
		table[0] = new String[] {"key", "value", "time"};
	 
		try {
			//read in csv, and add keys and values to the red black tree
			br = new BufferedReader(new FileReader("src/UPC.csv"));
			while ((line = br.readLine()) != null) {
	 
				String[] value = line.split(split);
	 
				String description = "";
				//number of columns vary
				for (int i = 1; i < value.length; i++){
					if (!value[i].equals("")){
						String s = value[i].trim();//trim whitespace then add whitespace to avoid messy output
						description += s + " ";//because not all entries are formatted the same way.
					}
				}
				
				t.put((long) Double.parseDouble(value[0]), description);
	 
			}
			
			br.close();
	 
			br = new BufferedReader(new FileReader("src/input.dat"));
			//read in upc's from input.dat, search for node and print the description of the item
			long time = 0;
			int c = 1;
			while ((line = br.readLine()) != null) {
				
				long start = System.nanoTime();
				HashElement<String> ss = t.get((long) Integer.parseInt(line));
				String s = "Null";
				if (ss != null){
					s = ss.value;
				}
				
				long end = System.nanoTime();
				time += (end-start);
				//add output to table
				table[c] = new String[] {line, s, (new Long(end-start)).toString()};
				c++;
				
			}
			//print table of values
			for (Object[] row : table) {
			    System.out.format("%15s%60s%15s\n", row);
			}
			System.out.println("total time for search : " + time);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			t = null; //"Clean" for use by garbage collector
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void linear(){
		
		HashMap<String> t = new HashMap<>(100000000);
		t.setHashType(t.LINEAR);
		
		BufferedReader br = null;
		String line = "";
		String split = ",";//used to separate by comma
		String[][] table = new String[16][];//table for output
		table[0] = new String[] {"key", "value", "time"};
	 
		try {
			//read in csv, and add keys and values to the red black tree
			br = new BufferedReader(new FileReader("src/UPC.csv"));
			while ((line = br.readLine()) != null) {
	 
				String[] value = line.split(split);
	 
				String description = "";
				//number of columns vary
				for (int i = 1; i < value.length; i++){
					if (!value[i].equals("")){
						String s = value[i].trim();//trim whitespace then add whitespace to avoid messy output
						description += s + " ";//because not all entries are formatted the same way.
					}
				}
				
				t.put((long) Double.parseDouble(value[0]), description);
	 
			}
			
			br.close();
	 
			br = new BufferedReader(new FileReader("src/input.dat"));
			//read in upc's from input.dat, search for node and print the description of the item
			long time = 0;
			int c = 1;
			while ((line = br.readLine()) != null) {
				
				long start = System.nanoTime();
				String s = t.get(Long.parseLong(line)).value;
				long end = System.nanoTime();
				time += (end-start);
				//add output to table
				table[c] = new String[] {line, s, (new Long(end-start)).toString()};
				c++;
				
			}
			//print table of values
			for (Object[] row : table) {
			    System.out.format("%15s%60s%15s\n", row);
			}
			System.out.println("total time for search : " + time);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			t = null;//clean
			//System.gc();
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	public static void quadratic(){
		
		HashMap<String> t = new HashMap<>(200005721);
		t.setHashType(t.QUADRATIC);
		
		BufferedReader br = null;
		String line = "";
		String split = ",";//used to separate by comma
		String[][] table = new String[16][];//table for output
		table[0] = new String[] {"key", "value", "time"};
	 
		try {
			//read in csv, and add keys and values to the red black tree
			br = new BufferedReader(new FileReader("src/UPC.csv"));
			while ((line = br.readLine()) != null) {
	 
				String[] value = line.split(split);
	 
				String description = "";
				//number of columns vary
				for (int i = 1; i < value.length; i++){
					if (!value[i].equals("")){
						String s = value[i].trim();//trim whitespace then add whitespace to avoid messy output
						description += s + " ";//because not all entries are formatted the same way.
					}
				}
				
				t.put((long) Double.parseDouble(value[0]), description);
	 
			}
			
			br.close();
	 
			br = new BufferedReader(new FileReader("src/input.dat"));
			//read in upc's from input.dat, search for node and print the description of the item
			long time = 0;
			int c = 1;
			while ((line = br.readLine()) != null) {
				
				long start = System.nanoTime();
				String s = t.get(Long.parseLong(line)).value;
				long end = System.nanoTime();
				time += (end-start);
				//add output to table
				table[c] = new String[] {line, s, (new Long(end-start)).toString()};
				c++;
				
			}
			//print table of values
			for (Object[] row : table) {
			    System.out.format("%15s%60s%15s\n", row);
			}
			System.out.println("total time for search : " + time);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	/*
	public static void redBlack(){
		
		RBTree<Long, String> t = new RBTree<Long, String>();
		
		BufferedReader br = null;
		String line = "";
		String split = ",";//used to separate by comma
		String[][] table = new String[16][];//table for output
		table[0] = new String[] {"key", "value", "time"};
	 
		try {
			//read in csv, and add keys and values to the red black tree
			br = new BufferedReader(new FileReader("src/UPC.csv"));
			while ((line = br.readLine()) != null) {
	 
				String[] value = line.split(split);
	 
				String description = "";
				//number of columns vary
				for (int i = 1; i < value.length; i++){
					if (!value[i].equals("")){
						String s = value[i].trim();//trim whitespace then add whitespace to avoid messy output
						description += s + " ";//because not all entries are formatted the same way.
					}
				}
				
				t.rBInsert((long) Double.parseDouble(value[0]), description);
	 
			}
			
			br.close();
	 
			br = new BufferedReader(new FileReader("src/input.dat"));
			//read in upc's from input.dat, search for node and print the description of the item
			long time = 0;
			int c = 1;
			while ((line = br.readLine()) != null) {
				
				long start = System.nanoTime();
				RBNode<Long, String> node = t.search((long)Double.parseDouble(line));
				long end = System.nanoTime();
				time =+ (end-start);
				//add output to table
				table[c] = new String[] {node.key.toString(), node.value, (new Long(end-start)).toString()};
				c++;
				
			}
			//print table of values
			for (Object[] row : table) {
			    System.out.format("%15s%60s%15s\n", row);
			}
			
			System.out.println("total time for search : " + time);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}*/
}
