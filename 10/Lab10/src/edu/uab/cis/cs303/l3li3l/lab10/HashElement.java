package edu.uab.cis.cs303.l3li3l.lab10;

public class HashElement<V> {
	public Long key;
	public V value;
	
	public HashElement(Long key, V value) {
		this.key = key;
		this.value = value;
	}

}
