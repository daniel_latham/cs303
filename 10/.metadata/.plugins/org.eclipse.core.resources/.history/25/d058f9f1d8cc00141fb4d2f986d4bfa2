package edu.uab.cis.cs303.l3li3l.lab10;

public class HashMap<V> {
	public final int size;
	private HashElement<V>[] he;
	
	public int hashType = this.REGULAR;
	
	public final int REGULAR = 0;
	public final int LINEAR = 1;
	public final int QUADRATIC = 2;
	
	public final int notFound = -1;
	
	@SuppressWarnings("unchecked")
	public HashMap(int size){
		this.size = size;
		this.he = new HashElement[this.size];
	}
	
	public int getHashType(){
		return this.hashType;
	}
	
	public void setHashType(int hashType){
		assert hashType >= 0 && hashType <= 2: "Use integer value i such that 0 <= i <= 2";
		this.hashType = hashType;
	}
	
	////////////////////////Q2 Hashing/////////////////////////////
	/**
	 * HashFunction for putting, returns int notFound if the array is full
	 * @param key
	 * @param value 
	 * @return index
	 */
	private void hashPut(Long key, V value){
		HashElement<V> h = new HashElement<>(key, value);
		int index  = (int) (key % this.size);
		
		if (he[index] == null || he[index].key == h.key){
			he[index] = h;
		}
		else {
			
			int c = (7 * index + 1) % size;
			while (he[c] != null && he[c].key != h.key){
				c = (7 * c + 1) % size;
			}
			he[c] = h;
		}
		
	}
	
	/**
	 * HashFunction for getting, returns int notFound if key not found
	 * @param key
	 * @return index
	 */
	private HashElement<V> hashGet(Long key){
		int index  = (int) (key % this.size);
		
		if (he[index] == null || he[index].key == key){ 
			return he[index];
		}
		int c = (7 * index + 1) % size;
		while (he[c] != null && he[c].key != key){
			c = (7 * c + 1) % size;
		}
		return he[c];
		
	}
	///////////////////////////////////////////////////////////
	
	////////////////////////LINEAR/////////////////////////////
	/**
	 * LinearProbe for putting
	 * @param key
	 * @param value 
	 * @return index
	 */
	private void linearProbePut(Long key, V value){
		HashElement<V> h = new HashElement<>(key, value);
		int index  = (int) (key % this.size);
		
		if (he[index] == null || he[index].key == h.key){
			he[index] = h;
		}
		else {
			
			int c = (index + 1) % size;
			while (he[c] != null && he[c].key != h.key){
				c = (c + 1) % size;
			}
			he[c] = h;
		}
	}
	/**
	 * LinearProbe for getting
	 * @param key
	 * @return index
	 */
	private HashElement<V> linearProbeGet(Long key){
		int index  = (int) (key % this.size);
		
		if (he[index] == null || he[index].key == key){ 
			return he[index];
		}
		int c = (index + 1) % size;
		while (he[c] != null && he[c].key != key){
			c = (c + 1) % size;
		}
		return he[c];
		
	}
	/////////////////////////////////////////////
	////////////////QUADRATIC////////////////////
	/**
	 * QuadraticProbe for putting
	 * @param key
	 * @param value 
	 * @return index
	 */
	private void quadraticProbePut(Long key, V value){
		HashElement<V> h = new HashElement<>(key, value);
		int index  = (int) (key % this.size);
		
		if (he[index] == null || he[index].key == h.key){
			he[index] = h;
		}
		else {
			int t = 1;
			int c = index;
			while (he[c] != null && he[c].key != h.key){
				c = (int) ((index + Math.pow(t, 2)) % size);
				t+=1;
			}
			he[c] = h;
		}
	}
	/**
	 * QuadraticProbe for getting
	 * @param key
	 * @return index
	 */
	private HashElement<V> quadraticProbeGet(Long key){
		int index  = (int) (key % this.size);
		
		if (he[index] == null || he[index].key == key){ 
			return he[index];
		}
		int t = 1;
		int c = index;
		while (he[c] != null && he[c].key != key){
			c = (int) ((index + Math.pow(t, 2)) % size);
			t+=1;
		}
		return he[c];
		
	}
	////////////////////////////////////////////
	
	public void put(Long key, V value){
		
		if (this.hashType == this.REGULAR) {
			hashPut(key, value);
		} else if (this.hashType == this.LINEAR) {
			linearProbePut(key, value);
		} else {
			quadraticProbePut(key, value);
		}
	}
	
	public HashElement<V> get(Long key){
		
		if (this.hashType == this.REGULAR) {
			return hashGet(key);
		} else if (this.hashType == this.LINEAR) {
			return linearProbeGet(key);
		} else {
			return quadraticProbeGet(key);
		}
	}
}
