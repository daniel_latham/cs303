package edu.uab.cis.cs303.l3li3l.lab10;

public class HashMap<V> {
	public final int size;
	private HashElement<V>[] he;
	
	private int hashType = this.REGULAR;
	
	public final int REGULAR = 0;
	public final int LINEAR = 1;
	public final int QUADRATIC = 2;
	
	public final int notFound = -1;
	
	@SuppressWarnings("unchecked")
	public HashMap(int size){
		this.size = size;
		this.he = new HashElement[this.size];
	}
	
	public int getHashType(){
		return this.hashType;
	}
	
	public void setHashType(int hashType){
		assert hashType >= 0 && hashType <= 2: "Use integer value i such that 0 <= i <= 2";
		this.hashType = hashType;
	}
	
	////////////////////////Q2 Hashing/////////////////////////////
	/**
	 * HashFunction for putting
	 * @param key
	 * @return index
	 */
	private int hashPut(Double key){
		int index = (int) (key % this.size);
		int k = 0;
		while (he[index] != null && he[index].key != key && k <= this.size) {
			index = (7*index+1) % this.size;
			k+=1;
		}
		if(he[index].key == key){
			return index;
			
		return this.notFound;
	}
	
	/**
	 * HashFunction for getting
	 * @param key
	 * @return index
	 */
	private int hashGet(Double key){
		int index = (int) (key % this.size);
		int k = 0;
		while (he[index].key != key && k <= this.size){
			index = (7*index + 1) % this.size;
			k+=1;
		}
		return index;
	}
	///////////////////////////////////////////////////////////
	
	////////////////////////LINEAR/////////////////////////////
	/**
	 * LinearProbe for putting
	 * @param key
	 * @return index
	 */
	private int linearProbePut(Double key){
		int index = (int) (key % this.size);
		int i = 1;
		while (he[index] != null && he[index].key != key) {
			index = (index+i) % this.size;
			i+=1;
		}
		return index;
	}
	/**
	 * LinearProbe for getting
	 * @param key
	 * @return index
	 */
	private int linearProbeGet(Double key){
		int index = (int) (key % this.size);
		int i = 1;
		while (he[index].key != key){
			index = (index + i) % this.size;
			i+=1;
		}
		return index;
	}
	/////////////////////////////////////////////
	////////////////QUADRATIC////////////////////
	/**
	 * QuadraticProbe for putting
	 * @param key
	 * @return index
	 */
	private int quadraticProbePut(Double key){
		int index = (int) (key % this.size);
		int i = 1;
		while (he[index] != null && he[index].key != key) {
			index = (index+(i*i)) % this.size;
			i+=1;
		}
		return index;
	}
	/**
	 * QuadraticProbe for getting
	 * @param key
	 * @return index
	 */
	private int quadraticProbeGet(Double key){
		int index = (int) (key % this.size);
		int i = 1;
		//int k = 1;
		while (he[index].key != key){
			index = (index + i*i) % this.size;
			i+=1;
		}
		return index;
	}
	////////////////////////////////////////////
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void put(Double key, V value){
		int index;
		
		if (this.hashType == this.REGULAR) {
			index = hashPut(key);
		} else if (this.hashType == this.LINEAR) {
			index = linearProbePut(key);
		} else {
			index = quadraticProbePut(key);
		}
		
		he[index] = new HashElement(key, value);
	}
	
	public HashElement<V> get(Double key){
		int index;
		
		if (this.hashType == this.REGULAR) {
			index = hashGet(key);
		} else if (this.hashType == this.LINEAR) {
			index = linearProbeGet(key);
		} else {
			index = quadraticProbeGet(key);
		}
		
		return he[index];
	}
}
