package edu.uab.cis.cs303.l3li3l.lab10;

public class HashMap {
	public int size;
	HashElement[] he;
	
	public HashMap(int size){
		this.size = size;
		he = new HashElement[this.size];
	}
	
	/////////////////////////////////////////////////////
	/**
	 * HashFunction for putting
	 * @param key
	 * @return index
	 */
	public int hashPut(int key){
		int index = key % this.size;
		while (he[index] != null && he[index].key != key) {
			index = (7*index+1) % this.size;
		}
		return index;
	}
	
	/**
	 * HashFunction for getting
	 * @param key
	 * @return index
	 */
	public int hashGet(int key){
		int index = key % this.size;
		while (he[index].key != key){
			index = (7*index + 1) % this.size;
		}
		return index;
	}
	///////////////////////////////////////////////////////////
	
	////////////////////////LINEAR/////////////////////////////
	/**
	 * LinearProbe for putting
	 * @param key
	 * @return index
	 */
	public int linearProbePut(int key){
		int index = key % this.size;
		while (he[index] != null && he[index].key != key) {
			index = (index+1) % this.size;
		}
		return index;
	}
	/**
	 * LinearProbe for getting
	 * @param key
	 * @return index
	 */
	public int linearProbeGet(int key){
		int index = key % this.size;
		while (he[index].key != key){
			index = (index + 1) % this.size;
		}
		return index;
	}
	/////////////////////////////////////////////
	
	
	public void put(int key, String value){
		int index = hashPut(key);
		he[index] = new HashElement(key, value);
	}
	
	public HashElement get(int key){
		int index = hashGet(key);
		return he[index];
	}
}
