package edu.uab.cis.cs303.l3li3l.lab10;

public class HashElement {
	public int key;
	public String value;
	
	public HashElement(int key, String value) {
		this.key = key;
		this.value = value;
	}

}
