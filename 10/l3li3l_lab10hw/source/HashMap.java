package lab10;

/**
 * A hash table (hash map) is a data structure used to implement 
 * an associative array, a structure that can map keys to values. 
 * A hash table uses a hash function to compute an index into an array 
 * of buckets or slots, from which the correct value can be found.
 * 
 * Ideally, the hash function will assign each key to a unique bucket,
 * but this situation is rarely achievable in practice (usually some 
 * keys will hash to the same bucket). Instead, most hash table designs 
 * assume that hash collisions—different keys that are assigned 
 * by the hash function to the same bucket—will occur and must be 
 * accommodated in some way.
 * 
 * In a well-dimensioned hash table, the average cost (number of instructions) 
 * for each lookup is independent of the number of elements stored in the table. 
 * Many hash table designs also allow arbitrary insertions and deletions of 
 * key-value pairs, at (amortized) constant average cost per operation.
 * In many situations, hash tables turn out to be more efficient than search 
 * trees or any other table lookup structure. For this reason, they are 
 * widely used in many kinds of computer software, particularly for associative 
 * arrays, database indexing, caches, and sets.
 * 
 * This HashMap uses three different methods for inserting and retrieving data
 * from the map. 'hashGet' and 'hashPut' use a weak hashing algorithm to place on
 * the map while 'linearGet' and 'linearPut' use linear arithmetic(index += 1) and
 * 'quadraticGet' and 'quadraticPut' use quadratic arithmetic(index += (n^2); n+=1;)
 * 
 * This project was tested on Ubuntu Linux Kernel v.3.8.11, armv7l 
 * Java(TM) SE Runtime Environment (build 1.8.0_33-b05)
 * Thu Mar 12 20:35:43 PDT 2015
 * 
 * @author Daniel Latham
 *
 */
public class HashMap {

	private final int size;
	private HashElement[] map;
	
	public HashMap(int size){
		this.size = size;
		this.map = new HashElement[this.size];
	}
	
	/**
	 * Puts HashElement with key/value somewhere in the hashmap with specification
	 * below
	 * @param key Unique key (long) ID used for searching
	 * @param value String param that actually holds the data
	 */
	public void hashPut(long key, String value){
		
		HashElement h = new HashElement(key, value);
		int index = (int)(key % this.size);
		
		if (map[index] == null || map[index].key == h.key){
			map[index] = h;
		}
		else {
			
			int nIndex = (7 * index + 1) % this.size;
			while (map[nIndex] != null && map[nIndex].key != h.key){
				nIndex = (7 * nIndex + 1) % this.size;
			}
				map[nIndex] = h;
		}
		
	}
	
	/**
	 * Returns a String value for the value of the hashmap user is trying to get
	 * @param key HashElement.key associated, unique
	 * @return String HashElement.value,
	 */
	public String hashGet(long key){
		
		int index = (int)(key % this.size);
		
		if (map[index] == null){
			return null;
		}
		else if (map[index].key == key){
			return map[index].value;
		}
		else {
			int nIndex = (7 * index + 1) % this.size;
			while (map[nIndex] != null && map[nIndex].key != key){
				nIndex = (7 * nIndex + 1) % this.size;
			}
				return map[nIndex].value;
		}
	}
	/**
	 * Puts HashElement with key/value somewhere in the hashmap with specification
	 * below
	 * @param key Unique key (long) ID used for searching
	 * @param value String param that actually holds the data
	 */
	public void linearPut(long key, String value){
		
		HashElement h = new HashElement(key, value);
		int index = (int)(key % this.size);
		
		if (map[index] == null || map[index].key == h.key){
			map[index] = h;
		}
		else {
			int offset = 1;
			int nIndex = index;
			while (map[nIndex] != null && map[nIndex].key != h.key){
				nIndex = nIndex + offset % this.size;
				offset++;
			}
			map[nIndex] = h;
		}
	}
	/**
	 * Returns a String value for the value of the hashmap user is trying to get
	 * @param key HashElement.key associated, unique
	 * @return String HashElement.value,
	 */
	public String linearGet(long key){
		
		int index = (int)(key % this.size);
		
		if (map[index] == null){
			return null;
		}
		else if (map[index].key == key){
			return map[index].value;
		}
		else {
			int offset = 1;
			int nIndex = index;
			while (map[nIndex] != null && map[nIndex].key != key){
				nIndex = (nIndex + offset) % this.size;
				offset++;
			}
			return map[nIndex].value;
		}
	}
	/**
	 * Puts HashElement with key/value somewhere in the hashmap with specification
	 * below
	 * @param key Unique key (long) ID used for searching
	 * @param value String param that actually holds the data
	 */
	public void quadraticPut(long key, String value){
		
		HashElement h = new HashElement(key, value);
		int index = (int)(key % this.size);
		
		if (map[index] == null || map[index].key == h.key){
			map[index] = h;
		}
		else {
			int offset = 1;
			int nIndex = index;
			while (map[nIndex] != null && map[nIndex].key != h.key){
				nIndex = (nIndex + (int)Math.pow(offset, 2)) % this.size;
				offset++;
			}
			map[nIndex] = h;
		}
		
	}
	/**
	 * Returns a String value for the value of the hashmap user is trying to get
	 * @param key HashElement.key associated, unique
	 * @return String HashElement.value,
	 */
	public String quadraticGet(long key){
		int index = (int)(key % this.size);
		
		if (map[index] == null){
			return null;
		}
		else if (map[index].key == key){
			return map[index].value;
		}
		else {
			int offset = 1;
			int nIndex = index;
			while (map[nIndex] != null && map[nIndex].key != key){
				nIndex = (nIndex + (int)Math.pow(offset, 2)) % this.size;
				offset++;
			}
			return map[nIndex].value;
		}
	}
}
