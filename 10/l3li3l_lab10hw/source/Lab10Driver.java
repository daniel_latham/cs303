package lab10;

public class Lab10Driver {

	public static void main(String[] args){
	
		HashMap hm = new HashMap(1000);
		hm.quadraticPut(50, "a");
		hm.quadraticPut(150, "b");
		hm.quadraticPut(250, "c");
		hm.quadraticPut(350, "d");
		hm.quadraticPut(450, "e");
		
		
		System.out.println(hm.quadraticGet(50));
		System.out.println(hm.quadraticGet(150));
		System.out.println(hm.quadraticGet(250));
		System.out.println(hm.quadraticGet(350));
		System.out.println(hm.quadraticGet(450));
	}
	
}
