package lab10;

/**
 * HashElement class, stores data as a String 'value' and unique key as a long
 * @author Daniel Latham
 *
 */
public class HashElement {

	public long key;
	public String value;
	
	public HashElement(long key, String value){
		this.key = key;
		this.value = value;
	}
}
